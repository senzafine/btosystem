<?php

namespace Deployer;

/**
 * Require autoload to avoid issues with Dotenv not working
 */
require_once __DIR__.'/../vendor/autoload.php';

use Dotenv;

/**
 * Return the WordPress URL from the local .env file
 * Return an error if not found
 */
$getLocalEnv = function () {
    $localEnv = new Dotenv\Dotenv(get('local_root'), '.env');
    $localEnv->overload();
    $localUrl = getenv('WP_HOME');

    if (! $localUrl) {
        writeln("<error>WP_HOME variable not found in local .env file</error>");
        return false;
    }

    return $localUrl;
};

/*
 * Download the remote .env file to a local tmp file to extract data
 * Return the remote WordPress URL from the extracted data
 * Return an error if not found
 */
$getRemoteEnv = function () {
    $tmpEnvFile = get('local_root') . '/.env.remote';
    download(get('deploy_path') . '/shared/.env', $tmpEnvFile);
    $remoteEnv = new Dotenv\Dotenv(get('local_root'), '.env.remote');
    $remoteEnv->overload();
    $remoteUrl = getenv('WP_HOME');

    /* Remove the temp file */
    runLocally("rm {$tmpEnvFile}");

    if (! $remoteUrl) {
        writeln("<error>WP_HOME variable not found in remote .env file</error>");
        return false;
    }

    return $remoteUrl;
};

/**
 * Remove the protocol and trailing slash from submitted url
 */
$urlToDomain = function ($url) {
    return preg_replace('/^https?:\/\/(.+)/i', '$1', rtrim($url, "/"));
};


/**
 * Tasks
 */
desc('Pull database from remote server and import it locally, after having made a backup of local database');
task('pull:db', function () use ($getLocalEnv, $getRemoteEnv, $urlToDomain) {
    /* Export remote database */
    $exportFilename = 'database_remote.sql';
    $exportAbsFile  = get('deploy_path') . '/' . $exportFilename;
    writeln("<comment>Exporting remote database to {$exportAbsFile}</comment>");
    run("cd {{current_path}} && wp db export {$exportAbsFile}");

    /* Download remote database */
    $downloadedExport = get('local_root') . '/' . $exportFilename;
    writeln("<comment>Downloading remote database to {$downloadedExport}</comment>");
    download($exportAbsFile, $downloadedExport);

    /* Detele exported file on remote server */
    writeln("<comment>Deleting {$exportAbsFile} on remote server</comment>");
    run("rm {$exportAbsFile}");

    /* Create backup of local database */
    $backupFilename = 'database_local.sql';
    $backupAbsFile  = get('local_root') . '/' . $backupFilename;
    writeln("<comment>Saving backup of local database to {$backupAbsFile}</comment>");
    runLocally("wp db export {$backupFilename}");

    /* Empty local database */
    writeln("<comment>Emptying local database</comment>");
    runLocally("wp db reset");

    /* Import remote database file */
    writeln("<comment>Importing {$downloadedExport}</comment>");
    runLocally("wp db import {$exportFilename}");

    /* Load remote .env file and get remote WP URL */
    if (! $remoteUrl = $getRemoteEnv()) {
        return;
    }

    /* Load local .env file and get local WP URL */
    if (! $localUrl = $getLocalEnv()) {
        return;
    }

    /* Get domain without protocol and trailing slash */
    $localDomain = $urlToDomain($localUrl);
    $remoteDomain = $urlToDomain($remoteUrl);

    /*
     * Update URLs in local database
     * In a multisite environment, the DOMAIN_CURRENT_SITE in the .env file uses the new remote domain
     * In the DB however, this new remote domain doesn't exist yet before search-replace,
     * so we have to specify the old (remote) domain as --url parameter.
     */
    writeln("<comment>Updating URLs in the local database</comment>");
    // runLocally("wp search-replace '{$remoteUrl}' '{$localUrl}' --skip-themes --url='{$remoteDomain}' --network");
    runLocally("wp search-replace '{$remoteUrl}' '{$localUrl}' --skip-themes");

    /* Replace domain (multisite WP also uses domains without protocol in DB) */
    // runLocally("wp search-replace '{$remoteDomain}' '{$localDomain}' --skip-themes --url='{$remoteDomain}' --network");
    runLocally("wp search-replace '{$remoteDomain}' '{$localDomain}' --skip-themes");

    /* Cleanup remote .sql file on local machine */
    writeln("<comment>Deleting {$downloadedExport} on local machine</comment>");
    // runLocally("rm {$downloadedExport}");
});

desc('Push local database to remote server and import it, after having made a backup of remote database');
task('push:db', function () use ($getLocalEnv, $getRemoteEnv, $urlToDomain) {
    /* Export local database */
    $exportFilename = 'database_local.sql';
    $exportAbsFile  = get('local_root') . '/' . $exportFilename;
    writeln("<comment>Exporting local database to {$exportAbsFile}</comment>");
    runLocally("wp db export {$exportFilename}");

    /* Upload local database to remote server */
    $uploadedExport = get('current_path') . '/' . $exportFilename;
    writeln("<comment>Uploading local database to {$uploadedExport} on remote server</comment>");
    upload($exportAbsFile, $uploadedExport);

    /* Delete exported local database */
    writeln("<comment>Deleting {$exportAbsFile} on local machine</comment>");
    runLocally("rm {$exportAbsFile}");

    /* Create backup of remote database */
    $backupFilename = 'database_remote.sql';
    $backupAbsFile  = get('deploy_path') . '/' . $backupFilename;
    writeln("<comment>Saving backup of remote database to {$backupAbsFile}</comment>");
    run("cd {{current_path}} && wp db export {$backupAbsFile}");

    /* Empty remote database */
    writeln("<comment>Emptying remote database</comment>");
    run("cd {{current_path}} && wp db reset");

    /* Import local database */
    writeln("<comment>Importing {$uploadedExport}</comment>");
    run("cd {{current_path}} && wp db import {$uploadedExport}");

    /* Load local .env file and get local WP URL */
    if (! $localUrl = $getLocalEnv()) {
        return;
    }

    /* Load remote .env file and get remote WP URL */
    if (! $remoteUrl = $getRemoteEnv()) {
        return;
    }

    /* Get domain without protocol and trailing slash */
    $localDomain = $urlToDomain($localUrl);
    $remoteDomain = $urlToDomain($remoteUrl);

    /*
     * Update URLs in database
     * In a multisite environment, the DOMAIN_CURRENT_SITE in the .env file uses the new remote domain.
     * In the DB however, this new remote domain doesn't exist yet before search-replace,
     * so we have to specify the old (local) domain as --url parameter.
     */
    writeln("<comment>Updating URLs in the remote database</comment>");
    run("cd {{current_path}} && wp search-replace \"{$localUrl}\" \"{$remoteUrl}\" --skip-themes --url='{$localDomain}' --network");

    /* Replace domain (multisite WP also uses domains without protocol in DB) */
    run("cd {{current_path}} && wp search-replace \"{$localDomain}\" \"{$remoteDomain}\" --skip-themes --url='{$localDomain}' --network");

    /* Cleanup uploaded file */
    writeln("<comment>Deleting {$uploadedExport} from remote server</comment>");
    run("rm {$uploadedExport}");
});
