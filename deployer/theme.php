<?php

namespace Deployer;

/**
 * Install theme dependencies
 */
desc('Run composer install on remote server');
task('theme:vendors', function () {
    writeln('<info>Installing theme dependencies</info>');
    run('cd {{release_path}}/{{theme_path}} && {{bin/composer}} {{composer_options}}');
});

desc('Compile the theme assets locally for production');
task('theme:compile', function () {
    writeln('<info>Compiling theme assets for production</info>');
    runLocally("cd {{local_root}}/{{theme_path}} && yarn run build");
});

desc('Updates remote assets with local assets');
task('theme:upload', function () {
    writeln('<info>Uploading compiled theme assets</info>');
    run('rm -rf {{current_path}}/{{theme_path}}/assets');
    upload('{{local_root}}/{{theme_path}}/assets', '{{release_path}}/{{theme_path}}');
});

desc('Builds assets and uploads them on remote server');
task('theme:deploy', [
    'theme:vendors',
    //'theme:compile',
    'theme:upload'
]);
