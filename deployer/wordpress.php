<?php

namespace Deployer;

/*
 * Runs Composer install for Bedrock
 */
desc('Installing Bedrock vendors');
task('wordpress:vendors', function () {
    run('cd {{release_path}} && {{bin/composer}} {{composer_options}}');
});

/**
 * Active plugins
 */
desc('Activate all WordPress plugins');
task('wordpress:plugins_active', function () {
    writeln('<info>Activating all installed plugins</info>');
    run('cd {{release_path}} && wp plugin activate --all');
});

/**
 * Copy .env file from previous release to current release
 * If previous .env file is not available create a new one:
 * - Generate a random token with a lenght of 64 characters
 * - Based on wp_generate_password() function
 */
desc('Makes sure, .env file for Bedrock is available');
task('wordpress:dotenv', function () {
    /* Copy .env file from previous release to current release if iut exists */
    if (has('previous_release')) {
        if (test("[ -f {{previous_release}}/.env ]")) {
            writeln('<info>Copy .env file from previous release to current release</info>');
            run("cp {{previous_release}}/.env {{release_path}}");
            return;
        }
    }

    /* If .env file doesn't exist create a new one */
    function generate_salt()
    {
        $chars              = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()-_ []{}<>~+ = ,.;:/?|';
        $char_option_length = strlen($chars) - 1;
        $password           = '';

        for ($i = 0; $i < 64; $i ++) {
            $password .= substr($chars, random_int(0, $char_option_length), 1);
        }

        return $password;
    }

    /* Keys that require a salt token */
    $salt_keys = [
        'AUTH_KEY',
        'SECURE_AUTH_KEY',
        'LOGGED_IN_KEY',
        'NONCE_KEY',
        'AUTH_SALT',
        'SECURE_AUTH_SALT',
        'LOGGED_IN_SALT',
        'NONCE_SALT',
    ];

    writeln('<comment>Generating .env file</comment>');

    /* Ask for credentials */
    $db_name   = ask(get('stage') . ' server WordPress database name');
    $db_user   = ask(get('stage') . ' server WordPress database user');
    $db_pass   = askHiddenResponse(get('stage') . ' server WordPress database password');
    $db_host   = ask(get('stage') . ' server WordPress database host', '127.0.0.1');
    $wp_env    = askChoice(get('stage') . ' server .env', ['development' => 'development', 'staging' => 'staging', 'production' => 'production'], 'staging');
    $wp_prot   = askChoice(get('stage') . ' server protocol', ['http' => 'http', 'https' => 'https'], 'http');
    $wp_domain = ask(get('stage') . ' server WordPress domain');
    ob_start();

    echo <<<EOL
DB_NAME='{$db_name}'
DB_USER='{$db_user}'
DB_PASSWORD='{$db_pass}'
DB_HOST='{$db_host}'
WP_ENV='{$wp_env}'
WP_HOME='{$wp_prot}://{$wp_domain}'
WP_SITEURL='{$wp_prot}://{$wp_domain}/wp'
DOMAIN_CURRENT_SITE='{$wp_domain}'
EOL;

    foreach ($salt_keys as $key) {
        echo $key . "='" . generate_salt() . "'" . PHP_EOL;
    }

    $content = ob_get_clean();

    run('echo "' . $content . '" > {{release_path}}/.env');
});
