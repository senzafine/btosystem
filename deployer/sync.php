<?php

namespace Deployer;

/**
 * Upload new and updated files and directories to the remote server
 * Delete non-existing files and directories from the remote server
 */
desc('Upload sync directories from local to server');
task('push:files', function () {
    writeln('<info>Syncing files and directories from local machine to remote server</info>');
    foreach (get('synced_dirs') as $localDir => $remoteDir) {
        upload($localDir, $remoteDir, $config = ['options' => ['--delete --exclude "backup_files.tar.gz"']]);
    };
});

/**
 * Download new and updated files and directories to the local machine
 * Delete non-existing files and directories from the local machine
 */
desc('Download sync directories from server to local');
task('pull:files', function () {
    writeln('<info>Syncing files and directories from remote server to local machine</info>');
    foreach (get('synced_dirs') as $localDir => $remoteDir) {
        download($remoteDir, $localDir, $config = ['options' => ['--delete --exclude "backup_files.tar.gz"']]);
    };
});

/**
 * Create a backup from the files and directories on the remote server
 */
desc('Create backup from sync directories on server');
task('backup:remote', function () {
    writeln('<info>Creating a backup of synced files and directories</info>');

    foreach (get('synced_dirs') as $localDir => $remoteDir) {
        $backupFilename = get('backup_file');

        /* If directory has a trailing slash, sync only its content */
        if (substr($remoteDir, - 1) == '/') {
            /* Add everything from synced directory to zip, but exclude previous backups */
            run("cd {$remoteDir} && tar -zcf {$backupFilename} .");
        } else {
            $backupDir = dirname($remoteDir);
            $dir       = basename($remoteDir);
            /* Add everything from synced directory to zip, but exclude previous backups */
            run("cd {$backupDir} && tar -zcf {$backupFilename} {$dir}");
        }
    };
});


/**
 * Create a backup from the files and directories on the local machine
 */
desc('Create backup from sync directories on local machine');
task('backup:local', function () {
    writeln('<info>Creating a backup of synced files and directories</info>');

    foreach (get('synced_dirs') as $localDir => $remoteDir) {
        $backupFilename = get('backup_file');

        /* If directory has a trailing slash, sync only its content */
        if (substr($localDir, - 1) == '/') {
            /* Add everything from synced directory to zip, but exclude previous backups */
            runLocally("cd {$localDir} && tar -zcf {$backupFilename} .");
        } else {
            $backupDir = dirname($localDir);
            $dir       = basename($localDir);
            /* Add everything from synced directory to zip, but exclude previous backups */
            runLocally("cd {$backupDir} && tar -zcf {$backupFilename}");
        }
    };
});

/**
 * Create a backup from the files and directories on the remote server
 * Upload new and updated files and directories to the remote server
 * Delete non-existing files and directories from the remote server
 */
desc('Upload sync directories from local to server after making backup of remote files');
task('push:files_backup', [
    'backup:remote',
    'push:files'
]);

/**
 * Create a backup from the files and directories on the local machine
 * Download new and updated files and directories to the local machine
 * Delete non-existing files and directories from the local machine
 */
desc('Download sync directories from server to local machine after making backup of local files');
task('pull:files_backup', [
    'backup:local',
    'pull:files'
]);
