# Spiral

Spiral is a WordPress stack for modern WordPress development.


## SETUP ENVIRONMENT
_**Replace the values between [ ] with your own info**_

1. Download and install **XAMPP** or **WAMPP**.

2. Add the local development address to `C:\Windows\System32\drivers\etc\hosts` file:
    ```
    127.0.0.1 [example.dev]
    ```

3. Create a database and a user for the project in MySQL or MariaDB

4. Download and install Git for Windows:
[https://git-for-windows.github.io/](https://git-for-windows.github.io/)

_**You'll need Git bash instead of the Windows command line for the rest fo the setup**_

5. Using Git bash, download and install Composer. Replace `[composer path]` with the address you want to install it, i.e. `C:\Composer`:
    ```
    php -r "readfile('https://getcomposer.org/installer');" > composer-setup.php
    php composer-setup.php --install-dir=[composer path] --filename=composer
    php -r "unlink('composer-setup.php');"
    ```

6. Again with Git bash, go to `[composer path]` and run this command to create a `.bat` file to be able to run simply `composer` on the command line:
    ```
    echo @php "%~dp0composer" %*>composer.bat
    ```

7. Add Composer to the Windows system path:

    a. Go to `Control Panel` > `System and Security` > `System` > `Advanced system settings` > `Advanced` > `Environment Variables...`

    b. In `System variables` select `Path`, the click `Edit...`

    c. In `Edit environment` variables click `New` and type the full path where you installed Composer, i.e. `C:\Composer`.

    d. Click `Ok` in all opens windows.

## Install WordPress

1. Create a folder for the project.

2. With Git bash, go into the folder you just created and clone the Git repository ofthe system (don't forget to type the dot at the end, otherwise the repo will be clone inside another folder in that directory):
    ```
    git clone https://gitlab.com/senzafine/btosystem.git .
    ```

3. Still inside the project folder, run this to install the Composer packages:
    ```
    composer install
    ```

4. Copy `.env.example` to `.env` in the same directory. You can use do it manually or use this command:
    ```
    cp .env.example .env
    ```

5. Open the `.env` file and update the environment variables with the values of your MySQL/MariaDB database table:
   * `DB_NAME`: Database name
   * `DB_USER`: Database user
   * `DB_PASSWORD`: Database password
   * `DB_HOST`: Database host
   * `DB_PREFIX`: Tables prefix
   * `WP_ENV`: Set to environment (`development`, `staging`, `production`). Leave it in `development` for local installs.
   * `WP_HOME`: Full URL to the project's address you added in the `hosts` file earlier (i.e. http://example.dev)
   * `WP_SITEURL`: Full URL to project's address, with the `/wp` added at the end for the WordPress folder (http://example.dev/wp).
   * `AUTH_KEY`, `SECURE_AUTH_KEY`, `LOGGED_IN_KEY`, `NONCE_KEY`, `AUTH_SALT`, `SECURE_AUTH_SALT`, `LOGGED_IN_SALT`, `NONCE_SALT`. You can use the [WordPress Salt Generator](https://api.wordpress.org/secret-key/1.1/salt/) to create those values.

6. Edit your Apache `httpd-vhost.conf` file inside the `Apache\conf\extra` folder to include the project address you setup earlier, and set the document root to the WordPress install:

    ```
    <VirtualHost *>
      DocumentRoot '[path to project]/web'
      ServerName [example.dev]
    </VirtualHost>
    ```

    If you get some permission errors when opening the website, add this to the `httpd-vhost.conf` files:

    ```
    <Directory '/'>
      Options Indexes FollowSymLinks
      AllowOverride All
      Require all granted
    </Directory>
    ```

7. With Git bash, go into the theme folder:
    ```
    cd web/app/themes/spiral
    ```

8. Install the Composer packages for the theme:
    ```
    composer install
    ```

9. Go to `[http://example.dev]` and complete the installation of WordPress as you would normally do.

10. Now you can access the WordPress Dashboard at `[http://example.dev]/wp/wp-admin`. Don't forget to go to `Appearence` > `Themes` and change the current theme to `Spiral`

***

## GIT BASH HELP

To navigate through folders you use the `cd` command.

1. To access a folder or a path type:
      ```
      cd [path]
      ```

2. To change drives:
    ```
    cd [letter]:
    ```
    i.e.
    ```
    cd e:
    ```

3. To go one folder up:
    ```
    cd ..
    ```

4. To list the contents of a folder:
    ```
    ls
    ```
