<?php

/*
Plugin Name:  Basic Site Setup
Plugin URI:   http://senzafine.net/coil/
Description:  Sets up default options for the WordPress installation
Version:      1.0.1
Author:       Yahzee Skellington
Author URI:   http://senzafine.net/
*/

/* Check if blog has been installed */
if (! is_blog_installed()) {
    return;
}

/* Set default options on plugin activation */
function wp_install_defaults($user_id)
{
    global $wpdb, $wp_rewrite, $current_site, $table_prefix;

    /* Build actual website root instead of the server document root value */
    $document_root_remote = realpath(__DIR__ . '/../../../../..');

    /* Set the upload folder depending on whether we're on a remote or local website */
    if (substr($document_root_remote, 0, 13) === '/home/vagrant') {
        $uploads_folder = $document_root_remote . '/web/media';
    } else {
        $uploads_folder = $document_root_remote . '/shared/web/media';
    }

    update_option('permalink_structure', '/%category%/%postname%/');
    update_option('upload_path', $uploads_folder);
    update_option('upload_url_path', getenv('WP_HOME') . '/media');
    update_option('uploads_use_yearmonth_folders', 0);
    update_option('start_of_week', 0);
    update_option('thumbnail_size_w', 0);
    update_option('thumbnail_size_h', 0);
    update_option('medium_size_w', 0);
    update_option('medium_size_h', 0);
    update_option('medium_large_size_w', 0);
    update_option('medium_large_size_h', 0);
    update_option('large_size_w', 0);
    update_option('large_size_h', 0);
}
