const webpack                 = require('webpack');
const path                    = require('path');
const CleanWebpackPlugin      = require('clean-webpack-plugin');
const ExtractTextPlugin       = require('extract-text-webpack-plugin');
const UglifyJSPlugin          = require('uglifyjs-webpack-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const cssnano                 = require('cssnano');
const SpriteLoaderPlugin      = require('svg-sprite-loader/plugin');
const CopyWebpackPlugin       = require('copy-webpack-plugin');
const ManifestPlugin          = require('webpack-assets-manifest');
const BrowserSyncPlugin       = require('browser-sync-webpack-plugin');

exports.clean = (path) => ({
  plugins: [
    new CleanWebpackPlugin([ path ], {
      root: process.cwd(),
      verbose: false,
      exclude: [ '.gitkeep' ],
      watch: false
    })
  ]
});

exports.browserSyncServer = ({ host, port, proxy, files, paths } = {}) => ({
  devServer: {
    compress: true,
    historyApiFallback: true,
    hot: true,
    noInfo: true,
    quiet: true
  },

  plugins: [
    new BrowserSyncPlugin(
      {
        host,
        port,
        proxy,
        files,

        watchOptions: {
          usePolling: true,
          interval: 500
        }
      },
      {
        reload: false
      }
    ),

    new webpack.NamedModulesPlugin(),

    new webpack.WatchIgnorePlugin([
      paths.node
    ]),

    new webpack.NoEmitOnErrorsPlugin()
  ]
});

exports.lintJavascript = ({ include, exclude, options }) => ({
  module: {
    rules: [
      {
        test: /\.js$/,
        include,
        exclude,
        enforce: 'pre',
        loader: 'eslint-loader',
        options
      }
    ]
  }
});

exports.loadScripts = ({ include, exclude, options }) => ({
  module: {
    rules: [
      {
        test: /\.js$/,
        include,
        exclude,
        loader: 'babel-loader',
        options
      }
    ]
  }
});

exports.minifyScripts = () => ({
  plugins: [
    new UglifyJSPlugin({
      sourceMap: true
    })
  ]
});

exports.lintStyles = ({ include, exclude }) => ({
  module: {
    rules: [
      {
        test: /\.css$/,
        include,
        exclude,
        enforce: 'pre',
        loader: 'postcss-loader',
        options: {
          plugins: () => ([
            require('stylelint')()
          ])
        }
      },

      {
        test: /\.styl$/,
        include,
        exclude,
        enforce: 'pre',
        loader: 'stylint-loader',
        options: {
          config: '.stylintrc'
        }
      }
    ]
  }
});

exports.loadStyles = ({ include, exclude, use } = {}) => ({
  module: {
    rules: [
      {
        test: /\.css$/,
        include,
        exclude,
        use
      },

      {
        test: /\.styl$/,
        include,
        exclude,
        use
      }
    ]
  }
});

exports.extractStyles = ({ include, exclude, filename, use }) => {
  const plugin = new ExtractTextPlugin({
    filename
  });

  return {
    module: {
      rules: [
        {
          test: /\.css$/,
          include,
          exclude,
          use: ExtractTextPlugin.extract({
            use,
            fallback: 'style-loader'
          })
        },

        {
          test: /\.styl$/,
          include,
          exclude,
          use: ExtractTextPlugin.extract({
            use,
            fallback: 'style-loader'
          })
        }
      ]
    },

    plugins: [
      plugin
    ]
  };
};

exports.autoprefix = () => ({
  loader: 'postcss-loader',
  options: {
    plugins: () => ([
      require('autoprefixer')()
    ])
  }
});

exports.purifyStyles = ({ paths }) => ({
  plugins: [
    new PurifyCSSPlugin({ paths })
  ]
});

exports.minifyStyles = ({ options }) => ({
  plugins: [
    new OptimizeCSSAssetsPlugin({
      cssProcessor: cssnano,
      cssProcessorOptions: options,
      canPrint: false
    })
  ]
});

exports.loadImages = ({ include, exclude, options } = {}) => ({
  module: {
    rules: [
      {
        test: /\.(svg|png|jpe?g|gif)$/,
        include,
        exclude,
        use: [
          {
            loader: 'file-loader',
            options
          },

          {
            loader: 'image-webpack-loader',
            options: {
              mozjpeg: {
                progressive: true
              },
              gifsicle: {
                interlaced: false
              },
              optipng: {
                optimizationLevel: 7
              },
              pngquant: {
                quality: '75-90',
                speed: 3
              }
            }
          }
        ]
      }
    ]
  }
});

exports.loadSprites = ({ include, exclude }) => {
  const plugin = new SpriteLoaderPlugin();

  return {
    module: {
      rules: [
        {
          test: /\.svg$/,
          include,
          exclude,

          loader: 'svg-sprite-loader',
          options: {
            extract: true,
            spriteFilename: 'images/sprite.svg'
          }
        }
      ]
    },

    plugins: [
      plugin
    ]
  };
}

exports.loadFonts = ({ include, exclude, options } = {}) => ({
  module: {
    rules: [
      {
        test: /\.(svg|eot|ttf|woff|woff2)(\?v=\d+\.\d+\.\d+)?$/,
        include,
        exclude,
        loader: 'file-loader',
        options
      }
    ]
  }
});

exports.sourceMaps = ({ type }) => ({
  devtool: type
});

exports.extractBundles = (bundles) => ({
  plugins: bundles.map((bundle) => (
    new webpack.optimize.CommonsChunkPlugin(bundle)
  ))
});

exports.copyAssets = () => ({
  plugins: [
    new CopyWebpackPlugin([
      {
        from: './resources/images',
        to: 'images'
      }
    ], {
      ignore: [
        'icons/*.*',
        'sprites/*.*',
        'slides/*.*',
        '*.ai',
        '.gitkeep'
      ]
    })
  ]
});

exports.assets = () => ({
  plugins: [
    new ManifestPlugin({
      output: 'manifest.json',
      writeToDisk: true,

      replacer: function(key, value) {
        if (typeof value === 'string') {
          return value;
        }

        const manifest = value;

        Object.keys(manifest).forEach((src) => {
          const sourcePath = path.basename(path.dirname(src));
          const targetPath = path.basename(path.dirname(manifest[src]));

          if (sourcePath === targetPath) {
            return;
          }

          manifest[`${targetPath}/${src}`] = manifest[src];
          delete manifest[src];
        });

        return manifest;
      }
    })
  ]
});

exports.setFreeVariable = (key, value) => {
  const env = {};

  env[key] = JSON.stringify(value);

  return {
    plugins: [
      new webpack.DefinePlugin(env)
    ]
  }
}
