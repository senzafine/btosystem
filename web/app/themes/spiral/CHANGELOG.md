# Spiral WordPress Template Change Log

## 3.0.0 - 2017-04-19

### Added

- Laravel Blade template engine

### Changed

- Renamed `resources` to `sources`
- Renamed `templates` to `views`

### Moved

- Webpack configuration and templates are now into `sources`

## 2.3.3 - 2017-04-3

### Changed

-Updated localization.

## 2.3.2 - 2017-03-29

- Reorganized style files.
- Updated button mixins.
- Updated variables calculations.
- Updated template organization.

## 2.0.0 - 2017-02-08

- Updated to Webpack 2.2

## 1.5.5 - 2017-10-16

- Integrated with Coil into one development stack

## 1.5.2 - 2016-10-03

- Updated to use BrowserSync along with Webpack Dev Server for testing environment

## 1.5.0 - 2016-09-24

- Moved project from gulp+Bower to Webpack for building assets

## 1.3.0 - 2016-09-12

- Moved and changed configuration files to reflect the structure of Sage v9.0

## 1.2.0 - 2016-09-12

- Updated with the latest Sage v8.3.2
