<?php
/**
 * Setup the tables for the system
 */

$charset_collate = $wpdb->get_charset_collate();

$methods_table    = $wpdb->prefix . 'methods';
$countries_table  = $wpdb->prefix . 'countries';
$categories_table = $wpdb->prefix . 'categories';
$scripts_table    = $wpdb->prefix . 'scripts';
$faqs_table       = $wpdb->prefix . 'faqs';

$query_methods = "CREATE TABLE IF NOT EXISTS $methods_table (
    id int(11) NOT NULL AUTO_INCREMENT,
    name text NOT NULL,
    PRIMARY KEY (id)
) $charset_collate;";

$query_countries = "CREATE TABLE IF NOT EXISTS $countries_table (
    id int(11) NOT NULL AUTO_INCREMENT,
    name text NOT NULL,
    PRIMARY KEY (id)
) $charset_collate;";

$query_categories = "CREATE TABLE IF NOT EXISTS $categories_table (
    id int(11) NOT NULL AUTO_INCREMENT,
    method text NOT NULL,
    country text NOT NULL,
    name text NOT NULL,
    PRIMARY KEY (id)
) $charset_collate;";

$query_scripts = "CREATE TABLE IF NOT EXISTS $scripts_table (
    id int(11) NOT NULL AUTO_INCREMENT,
    method int(11) NOT NULL,
    country text NOT NULL,
    category int(11) NOT NULL,
    title text NOT NULL,
    help text NOT NULL,
    text longtext NOT NULL,
    PRIMARY KEY (id)
) $charset_collate;";

$query_faqs = "CREATE TABLE IF NOT EXISTS $faqs_table (
    id int(11) NOT NULL AUTO_INCREMENT,
    title text NOT NULL,
    text longtext NOT NULL,
    PRIMARY KEY (id)
) $charset_collate;";

require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

dbDelta($query_methods);
dbDelta($query_countries);
dbDelta($query_categories);
dbDelta($query_scripts);
dbDelta($query_faqs);
