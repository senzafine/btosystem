<?php

namespace App;

/**
 * Change footer text
 */
add_filter('admin_footer_text', function () {
    echo "Made by <a href='http://senzafine.net'>Senzafine</a>. Powered by <a href='http://wordpres.org'>WordPress</a>.";
});

/**
 * Replace Admin Logo
 */
add_action('admin_head', function () {
    echo "<style type='text/css'>#header-logo {background-image: url(". get_bloginfo('template_directory') ."/images/logo.svg) !important;}</style>";
});

/**
 * Theme customizer
 */
add_action('customize_register', function (\WP_Customize_Manager $wp_customize) {
    // Add postMessage support
    $wp_customize->get_setting('blogname')->transport = 'postMessage';
    $wp_customize->selective_refresh->add_partial('blogname', [
        'selector' => '.brand',
        'render_callback' => function () {
            bloginfo('name');
        }
    ]);
});

/**
 * Customizer JS
 */
add_action('customize_preview_init', function () {
    wp_enqueue_script('spiral/customizer.js', asset_path('scripts/customizer.js'), ['customize-preview'], null, true);
});
