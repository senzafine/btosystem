<?php

/**
 * Dashboard Pages
 */
add_action('admin_menu', function () {
    add_menu_page('Manage Scripts', 'SMS', 'manage_options', 'sms-admin-scripts.php', 'sms_scripts_managemennt', 'dashicons-layout', 1);
    add_submenu_page('sms-admin-scripts.php', 'Manage Scripts', 'Scripts', 'manage_options', 'sms-admin-scripts.php', 'sms_scripts_managemennt');
    add_submenu_page('sms-admin-scripts.php', 'Manage FAQs', 'FAQs', 'manage_options', 'sms-admin-faqs.php', 'sms_faqs_managemennt');

    $current_user         = wp_get_current_user();
    $current_user_level   = get_user_meta($current_user->ID, 'sms_level', true);
    $current_user_country = get_user_meta($current_user->ID, 'sms_country', true);

    if ($current_user_level == 1) {
        add_submenu_page('sms-admin-scripts.php', 'Manage Settings', 'Settings', 'manage_options', 'sms-admin-settings.php', 'sms_settings_managemennt');
    }
});


/**
 * Script Management Page
 */
function sms_scripts_managemennt() {
    $current_user         = wp_get_current_user();
    $current_user_level   = get_user_meta($current_user->ID, 'sms_level', true);
    $current_user_country = get_user_meta($current_user->ID, 'sms_country', true);

    $id     = '';
    $title  = '';
    $output = '';

    if (! empty($script)) {
        $id    = $script[0]->id;
        $title = $script[0]->title;
    }
    ?>
    <div class="wrap sms scripts">
        <h1><?= __('Script Management', 'spiral'); ?></h1>

        <div id="message" class="alert-info"></div>

        <?php if ($current_user_level == 1) : ?>
            <hr>

            <section id="languages" class="accordion-section">
                <h3 class="accordion-header"><?= __('Languages', 'spiral'); ?></h3>
                <div class="accordion-content"></div>
            </section>

            <hr>

            <section id="methods" class="accordion-section">
                <h3 class="accordion-header"><?= __('Methods', 'spiral'); ?></h3>
                <div class="accordion-content"></div>
            </section>

            <hr>

            <section id="countries" class="accordion-section">
                <h3 class="accordion-header"><?= __('Countries', 'spiral'); ?></h3>
                <div class="accordion-content"></div>
            </section>

            <hr>

            <section id="categories" class="accordion-section">
                <h3 class="accordion-header"><?= __('Categories', 'spiral'); ?></h3>
                <div class="accordion-content"></div>
            </section>

        <?php endif; ?>

        <hr>

        <section id="script" class="accordion-section">
            <h3 class="accordion-header"><?= __('Scripts', 'spiral'); ?></h3>
            <div class="accordion-content">
                <div id="languages-dropdown"></div>
                <div id="methods-dropdown"></div>
                <div id="countries-dropdown"></div>
                <div id="categories-dropdown"></div>
                <div id="scripts-dropdown"></div>

                <form>
                    <h3><?= __('Add/Edit Script', 'spiral'); ?></h3>
                    <input type="hidden" name="script-id" value="<?= $id; ?>" id="script-id">
                    <label><?= __('Title', 'spiral'); ?></label>
                    <input type="text" name="script-title" placeholder="Title" value="<?= $title; ?>" id="script-title">
                    <label><?= __('Help', 'spiral'); ?></label>
                    <?php wp_editor('', 'script-help'); ?>
                    <label><?= __('Text', 'spiral'); ?></label>
                    <?php wp_editor('', 'script-text'); ?>
                    <div class="btn-group">
                        <button type="button" id="script-save"><?= __('Save Script', 'spiral'); ?></button>
                        <button type="button" id="script-delete"><?= __('Delete Script', 'spiral'); ?></button>
                    </div>
                </form>

                <button type="button" id="script-add"><?= __('Add New Script', 'spiral'); ?></button>
            </div>
        </section>
    </div>
    <?php
}


/**
 * FAQ Management Page
 */
function sms_faqs_managemennt() {
    global $wpdb;

    $faqs_table = $wpdb->prefix .'faqs';
    $faqs       = $wpdb->get_results("SELECT * FROM {$faqs_table}");

    $id    = '';
    $title = '';

    if (! empty($faq)) {
        $id    = $faq[0]->id;
        $title = $faq[0]->title;
    }
    ?>
    <div class="wrap sms faqs">
        <h1><?= __('FAQs Management', 'spiral'); ?></h1>

        <div id="message" class="alert-info"></div>

        <div id="faqs">
            <?php if (! empty($faqs) && $faqs !== '') : ?>
                <select name="faqs" id="faq-list">
                    <option value="">-- <?= __('Select a FAQ to edit', 'spiral'); ?> --</option>

                    <?php foreach ($faqs as $this_faq) : ?>
                        <option value="<?= $this_faq->id ;?>"><?= $this_faq->title; ?></option>
                    <?php endforeach; ?>
                </select>
            <?php else: ?>
                <h4><?= __('No FAQs saved. Add some FAQs first.', 'spiral'); ?></h4>
            <?php endif; ?>
        </div>

        <div id="faq">
            <h3><?= __('Add/Edit FAQ', 'spiral'); ?></h3>
            <input type="hidden" name="faq-id" value="<?= $id; ?>" id="faq-id">
            <label><?= __('Title', 'spiral'); ?></label>
            <input type="text" name="faq-title" placeholder="Title" value="<?= $title; ?>" id="faq-title">
            <label><?= __('Text', 'spiral'); ?></label>
            <?php wp_editor('', 'faq-text'); ?>
            <div class="btn-group">
                <button type="button" id="faq-save"><?= __('Save FAQ', 'spiral'); ?></button>
                <button type="button" id="faq-delete"><?= __('Delete FAQ', 'spiral'); ?></button>
            </div>
        </div>

        <button type="button" id="faq-add"><?= __('Add New FAQ', 'spiral'); ?></button>
    </div>
    <?php
}


/**
 * Settings Management Page
 */
function sms_settings_managemennt() {
    global $wpdb;

    $users           = get_users();
    $image_name      = get_option('sms_logo');
    $image_default   = get_stylesheet_directory_uri() .'/assets/images/logo.png';
    $color_primary   = get_option('color_primary');
    $color_secondary = get_option('color_secondary');
    $color_font      = get_option('color_font');

    if ($color_primary == '') {
        $color_primary = '#0066b3';
    }

    if ($color_secondary == '') {
        $color_secondary = '#1d407d';
    }

    if ($color_font == '') {
        $color_font = '#fefefe';
    }
    ?>
    <div class="wrap sms settings">
        <h1><?= __('Settings', 'spiral'); ?></h1>

        <div id="message" class="alert-info"></div>

        <section class="settings-logo">
            <h2><?= __('Logo', 'spiral'); ?></h2>

            <?php
            if (! empty($image_name)) {
                $image_attributes = wp_get_attachment_image_src($image_name, 'full');
                $image_src        = $image_attributes[0];
                $image_value      = $image_name;
            } else {
                $image_src   = $image_default;
                $image_value = '';
            }
            ?>

            <form method="post" class="upload">
                <img data-src="<?= $image_default; ?>" src="<?= $image_src; ?>" class="image-url">
                <input type="hidden" name="image-id" value="<?= $image_value; ?>" class="image-id">
                <div class="btn-group">
                    <button type="submit" class="image-upload"><?= __('Upload', 'spiral'); ?></button>
                    <button type="submit" class="image-save"><?= __('Save', 'spiral'); ?></button>
                    <button type="submit" class="image-remove">&times;</button>
                </div>
            </form>
        </section>

        <hr>

        <section class="settings-colors">
            <h2><?= __('Colors', 'spiral'); ?></h2>
            <ul>
                <li>
                    <span><?= __('Primary', 'spiral'); ?></span>
                    <input type="text" name="color-primary" data-default-color="<?= $color_primary; ?>" value="<?= $color_primary; ?>" class="color-select color-primary">
                    <button class="color-save"><?= __('Save', 'spiral'); ?></button>
                </li>

                <li>
                    <span class="color"><?= __('Secondary', 'spiral'); ?></span>
                    <input type="text" name="color-secondary" data-default-color="<?= $color_secondary; ?>" value="<?= $color_secondary; ?>" class="color-select color-secondary">
                    <button class="color-save"><?= __('Save', 'spiral'); ?></button>
                </li>

                <li>
                    <span class="color"><?= __('Font', 'spiral'); ?></span>
                    <input type="text" name="color-font" data-default-color="<?= $color_font; ?>" value="<?= $color_font; ?>" class="color-select color-font">
                    <button class="color-save"><?= __('Save', 'spiral'); ?></button>
                </li>
            </ul>
        </section>

        <hr>

        <section class="settings-users">
            <h2><?= __('Users', 'spiral'); ?></h2>

            <ul>
                <?php foreach ($users as $user) :?>
                    <li>
                        <input type="hidden" value="<?= $user->ID; ?>" class="user-id">
                        <span class="user"><?= $user->display_name; ?></span>
                        <label for "user-level"><?= __('Level'); ?></label>
                        <select name="user-level" class="user-level">
                            <option value="">-- <?= __('Select a user level', 'spiral'); ?> --</option>
                            <?php
                            $levels_table = $wpdb->prefix .'levels';
                            $levels       = $wpdb->get_results("SELECT * FROM {$levels_table}");

                            if (! empty($levels)) {
                                foreach ($levels as $level) {
                                    ?>
                                    <option value="<?= $level->id; ?>" <?= check_selected($level->id, get_user_meta($user->ID, 'sms_level', true)); ?>><?= $level->name; ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                        <label for "user-country"><?= __('Country'); ?></label>
                        <select name="user-country" class="user-country">
                            <option value="">-- <?= __('Select a country', 'spiral'); ?> --</option>
                            <option value="0" <?= check_selected(0, get_user_meta($user->ID, 'sms_country', true)); ?>><?= __('ALL COUNTRIES', 'spiral'); ?></option>
                            <?php
                            $countries_table = $wpdb->prefix .'countries';
                            $countries       = $wpdb->get_results("SELECT * FROM {$countries_table}");

                            if (! empty($countries)) {
                                foreach ($countries as $country) {
                                    ?>
                                    <option value="<?= $country->id; ?>" <?= check_selected($country->id, get_user_meta($user->ID, 'sms_country', true)); ?>><?= $country->name; ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                        <button class="user-save"><?= __('Save', 'spiral'); ?></button>
                    </li>
                <?php endforeach; ?>
            </ul>
        </section>
    </div>
    <?php
}


/**
 * Languages
 */

/** Get languages */
add_action('wp_ajax_get_languages', 'spiral_get_languages');
add_action('wp_ajax_nopriv_get_languages', 'spiral_get_languages');

function spiral_get_languages() {
    global $wpdb;

    $languages_table = $wpdb->prefix .'languages';
    $languages       = $wpdb->get_results("SELECT * FROM {$languages_table}");

    $output = '<table>';

    if (! empty($languages)) {
        foreach ($languages as $language) {
            $output .= language_row($language->id, $language->name);
        }

    } else {
        $output .= language_row();
    }

    $output .= '</table>';
    $output .= '<button type="button" class="language-add">'. __('Add another language', 'spiral') .'</button>';

    echo $output;

    wp_die();
}

function language_row($id = null, $name = null) {
    $output .= '
    <tr>
        <td>
            <input type="hidden" name="language-id" value="'. $id .'" class="language-id">
            <input type="text" name="language-name" value="'. $name .'" placeholder="'. __('Language name', 'spiral') .'" class="language-name">
        </td>
        <td>
            <div class="btn-group">
                <button type="button" class="language-save">'. __('Save', 'spiral') .'</button>
                <button type="button" class="language-delete">'. __('Delete', 'spiral') .'</button>
            </div>
        </td>
    </tr>';

    return $output;
}

/** Get languages dropdown */
add_action('wp_ajax_get_languages_dropdown', 'spiral_get_languages_dropdown');
add_action('wp_ajax_nopriv_get_languages_dropdown', 'spiral_get_languages_dropdown');

function spiral_get_languages_dropdown() {
    global $wpdb;

    $languages_table = $wpdb->prefix .'languages';
    $languages       = $wpdb->get_results("SELECT * FROM {$languages_table}");

    $output = '<select>';

    if (! empty($languages)) {
        $output .= '<option value="">--'. __('Select a Language', 'spiral') .'--</option>';

        foreach($languages as $language) {
            $output .= '<option value="'. $language->id .'">'. $language->name .'</option>';
        }
    } else {
        $output .= '<option>--'. __('First add a language', 'spiral') .'--</option>';
    }

    $output .= '</select>';

    echo $output;

    wp_die();
}

/** Get languages list */
function get_languages_list($selected = '') {
    global $wpdb;

    $languages_table = $wpdb->prefix .'languages';
    $languages       = $wpdb->get_results("SELECT * FROM {$languages_table}");

    $output = '<select name="category-language" class="category-language">';

    if (! empty($languages)) {
        $output .= '<option value="">--'. __('Select a Language') .'--</option>';

        foreach ($languages as $language) {
            $output .= '<option value="'. $language->id .'"'. check_selected($language->id, $selected) .'>'. $language->name .'</option>';
        }

        $output .= '</select>';

        return $output;
    }
}

/** Save a language */
add_action('wp_ajax_save_language', 'spiral_save_language');
add_action('wp_ajax_nopriv_save_language', 'spiral_save_language');

function spiral_save_language() {
    global $wpdb;

    $id   = $_POST['id'];
    $name = $_POST['name'];

    $languages_table = $wpdb->prefix .'languages';

    $data = [
        'name' => $name
    ];

    if (empty($id)) {
        $save_language = $wpdb->insert($languages_table, $data);
    } else {
        $save_language = $wpdb->update($languages_table, $data, ['id' => $id]);
    }

    if ($save_language) {
        echo __('Language successfully saved!', 'spiral');
    } else {
        echo __('There was an error saving the language. Data hasn\'t been changed.', 'spiral');
    }

    wp_die();
}

/** Delete a language */
add_action('wp_ajax_delete_language', 'spiral_delete_language');
add_action('wp_ajax_nopriv_delete_language', 'spiral_delete_language');

function spiral_delete_language() {
    global $wpdb;

    $id = $_POST['id'];

    if (! empty($id)) {
        $language_table  = $wpdb->prefix .'languages';
        $delete_language = $wpdb->delete($language_table, ['id' => $id]);

        if ($delete_language) {
            echo __('Language successfully deleted!', 'spiral');
        } else {
            echo __('There was an error deleting the language.', 'spiral');
        }
    } else {
        echo __('No language was selected to be deleted.', 'spiral');
    }

    wp_die();
}


/**
 * Methods
 */

/** Get methods */
add_action('wp_ajax_get_methods', 'spiral_get_methods');
add_action('wp_ajax_nopriv_get_methods', 'spiral_get_methods');

function spiral_get_methods() {
    global $wpdb;

    $methods_table = $wpdb->prefix .'methods';
    $methods       = $wpdb->get_results("SELECT * FROM {$methods_table}");

    $output = '<table>';

    if (! empty($methods)) {
        foreach($methods as $method) {
            $output .= method_row($method->id, $method->name);
        }
    } else {
        $output .= method_row();
    }

    $output .= '</table>';
    $output .= '<button type="button" class="method-add">'. __('Add another method', 'spiral') .'</button>';

    echo $output;

    wp_die();
}

function method_row($id = null, $name = null) {
    $output .= '
    <tr>
        <td>
            <input type="hidden" name="method-id" value="'. $id .'" class="method-id">
            <input type="text" name="method-name" value="'. $name .'" placeholder="'. __('Method Name', 'spiral') .'" class="method-name">
        </td>
        <td>
            <div class="btn-group">
                <button type="button" class="method-save">'. __('Save', 'spiral') .'</button>
                <button type="button" class="method-delete">'. __('Delete', 'spiral') .'</button>
            </div>
        </td>
    </tr>';

    return $output;
}

/** Get methods dropdown */
add_action('wp_ajax_get_methods_dropdown', 'spiral_get_methods_dropdown');
add_action('wp_ajax_nopriv_get_methods_dropdown', 'spiral_get_methods_dropdown');

function spiral_get_methods_dropdown() {
    global $wpdb;

    $methods_table = $wpdb->prefix .'methods';
    $methods       = $wpdb->get_results("SELECT * FROM {$methods_table}");

    $output = '<select>';

    if (! empty($methods)) {
        $output .= '<option value="">--'. __('Select a Method', 'spiral') .'--</option>';

        foreach($methods as $method) {
            $output .= '<option value="'. $method->id .'">'. $method->name .'</option>';
        }
    } else {
        $output .= '<option>--'. __('First add a method', 'spiral') .'--</option>';
    }

    $output .= '</select>';

    echo $output;

    wp_die();
}

/** Get methods list */
function get_methods_list($selected = '') {
    global $wpdb;

    $methods_table = $wpdb->prefix .'methods';
    $methods       = $wpdb->get_results("SELECT * FROM {$methods_table}");

    if (! empty($selected)) {
        $selected = json_decode($selected);
    }

    $output = '<select name="category-method" class="category-method">';

    if (! empty($methods)) {
        $output .= '<option value="">--'. __('Select a Method') .'--</option>';

        foreach ($methods as $method) {
            $output .= '<option value="'. $method->id .'"'. check_selected($method->id, $selected) .'>'. $method->name .'</option>';
        }

        $output .= '</select>';

    return $output;
    }
}

/** Save a method */
add_action('wp_ajax_save_method', 'spiral_save_method');
add_action('wp_ajax_nopriv_save_method', 'spiral_save_method');

function spiral_save_method() {
    global $wpdb;

    $id   = $_POST['id'];
    $name = $_POST['name'];

    $methods_table = $wpdb->prefix .'methods';

    $data = [
        'name' => $name
    ];

    if (empty($id)) {
        $save_method = $wpdb->insert($methods_table, $data);
    } else {
        $save_method = $wpdb->update($methods_table, $data, ['id' => $id]);
    }

    if ($save_method) {
        echo __('Method successfully saved!', 'spiral');
    } else {
        echo __('There was an error saving the method. Data hasn\'t been changed.', 'spiral');
    }

    wp_die();
}

/** Delete a method */
add_action('wp_ajax_delete_method', 'spiral_delete_method');
add_action('wp_ajax_nopriv_delete_method', 'spiral_delete_method');

function spiral_delete_method() {
    global $wpdb;

    $id = $_POST['id'];

    if (! empty($id)) {
        $methods_table = $wpdb->prefix .'methods';
        $delete_method = $wpdb->delete($methods_table, ['id' => $id]);

        if ($delete_method) {
            echo __('Method successfully deleted!', 'spiral');
        } else {
            echo __('There was an error deleting the method.', 'spiral');
        }
    } else {
        echo __('No method was selected to be deleted.', 'spiral');
    }

    wp_die();
}


/**
 * Countries
 */

/** Get countries */
add_action('wp_ajax_get_countries', 'spiral_get_countries');
add_action('wp_ajax_nopriv_get_countries', 'spiral_get_countries');

function spiral_get_countries() {
    global $wpdb;

    $countries_table = $wpdb->prefix .'countries';
    $countries       = $wpdb->get_results("SELECT * FROM {$countries_table}");

    $output = '<table>';

    if (! empty($countries)) {
        foreach($countries as $country) {
            $output .= country_row($country->id, $country->name);
        }
    } else {
        $output .= country_row();
    }

    $output .= '</table>';
    $output .= '<button type="button" class="country-add">'. __('Add another country', 'spiral') .'</button>';

    echo $output;

    wp_die();
}

function country_row($id = null, $name = null) {
    $output .= '
    <tr>
        <td>
            <input type="hidden" name="country-id" value="'. $id .'" class="country-id">
            <input type="text" name="country-name" value="'. $name .'" placeholder="'. __('Country Name', 'spiral') .'" class="country-name">
        </td>
        <td>
            <div class="btn-group">
                <button type="button" class="country-save">'. __('Save', 'spiral') .'</button>
                <button type="button" class="country-delete">'. __('Delete', 'spiral') .'</button>
            </div>
        </td>
    </tr>';

    return $output;
}

/** Get countries dropdown */
add_action('wp_ajax_get_countries_dropdown', 'spiral_get_countries_dropdown');
add_action('wp_ajax_nopriv_get_countries_dropdown', 'spiral_get_countries_dropdown');

function spiral_get_countries_dropdown() {
    global $wpdb, $current_user_country;

    $countries_table = $wpdb->prefix .'countries';
    $countries       = $wpdb->get_results("SELECT * FROM {$countries_table}");

    $output = '<select multiple>';
    $output .= '<option value="" selected>--'. __('Select a Country', 'spiral') .'--</option>';

    if (! empty($countries)) {
        foreach($countries as $country) {
            if ($country->id == $current_user_country || 0 == $current_user_country) {
                $output .= '<option value="'. $country->id .'">'. $country->name .'</option>';
            }
        }
    } else {
        $output .= '<option>--'. __('First add some countries', 'spiral') .'--</option>';
    }

    $output .= '</select>';

    echo $output;

    wp_die();
}

/** Get countries list */
function get_countries_list($selected = '') {
    global $wpdb;

    $countries_table = $wpdb->prefix .'countries';
    $countries       = $wpdb->get_results("SELECT * FROM {$countries_table}");

    if (! empty($selected)) {
        $selected = json_decode($selected);
    }

    if (! empty($countries)) {
        $output = '<select multiple name="category-country" class="category-country">';
        $output .= '<option value="">--'. __('Choose country', 'spiral') .'--</option>';

        foreach ($countries as $country) {
            $output .= '<option value="'. $country->id .'"'. check_selected($country->id, $selected) .'>'. $country->name .'</option>';
        }

        $output .= '</select>';
    }

    return $output;
}

/** Save a country */
add_action('wp_ajax_save_country', 'spiral_save_country');
add_action('wp_ajax_nopriv_save_country', 'spiral_save_country');

function spiral_save_country() {
    global $wpdb;

    $id   = $_POST['id'];
    $name = $_POST['name'];

    $countries_table = $wpdb->prefix .'countries';

    $data = [
        'name' => $name
    ];

    if (empty($id)) {
        $save_country = $wpdb->insert($countries_table, $data);
    } else {
        $save_country = $wpdb->update($countries_table, $data, ['id' => $id]);
    }

    if ($save_country) {
        echo __('Country successfully saved!', 'spiral');
    } else {
        echo __('There was an error saving the country. Data hasn\'t been changed.', 'spiral');
    }

    wp_die();
}

/** Delete a country */
add_action('wp_ajax_delete_country', 'spiral_delete_country');
add_action('wp_ajax_nopriv_delete_country', 'spiral_delete_country');

function spiral_delete_country() {
    global $wpdb;

    $id = $_POST['id'];

    if (! empty($id)) {
        $countries_table = $wpdb->prefix .'countries';
        $delete_country  = $wpdb->delete($countries_table, ['id' => $id]);

        if ($delete_country) {
            echo __('Country successfully deleted!', 'spiral');
        } else {
            echo __('There was an error deleting the country.', 'spiral');
        }
    } else {
        echo __('No country was selected to be delete.', 'spiral');
    }

    wp_die();
}


/**
* Categories
*/

/** Get categories */
add_action('wp_ajax_get_categories', 'spiral_get_categories');
add_action('wp_ajax_nopriv_get_categories', 'spiral_get_categories');

function spiral_get_categories() {
    global $wpdb;

    $categories_table = $wpdb->prefix .'categories';
    $categories       = $wpdb->get_results("SELECT * FROM {$categories_table}");

    $output = '<table>';

    if (! empty($categories)) {
        foreach($categories as $category) {
            $output .= category_row($category->id, $category->name, $category->language, $category->method, $category->country);
        }
    } else {
        $output .= category_row();
    }

    $output .= '</table>';
    $output .= '<button type="button" class="category-add">'. __('Add another category', 'spiral') .'</button>';

    echo $output;

    wp_die();
}

function category_row($id = null, $name = null, $language = null, $method = null, $country = null) {
    $output .= '
    <tr>
        <td>
            <input type="hidden" name="category-id" value="'. $id .'" class="category-id">
            <input type="text" name="category-name" value="'. $name .'" placeholder="'. __('Category name', 'spiral') .'" class="category-name">
        </td>
        <td>'. get_languages_list($language) .'</td>
        <td>'. get_methods_list($method) .'</td>
        <td>'. get_countries_list($country) .'</td>
        <td>
            <div class="btn-group">
                <button type="button" class="category-save">'. __('Save', 'spiral') .'</button>
                <button type="button" class="category-delete">'. __('Delete', 'spiral') .'</button>
            </div>
        </td>
    </tr>';

    return $output;
}

/** Get categories dropdown */
add_action('wp_ajax_get_categories_dropdown', 'spiral_get_categories_dropdown');
add_action('wp_ajax_nopriv_get_categories_dropdown', 'spiral_get_categories_dropdown');

function spiral_get_categories_dropdown() {
    global $wpdb;

    $language  = $_GET['language'];
    $method    = $_GET['method'];
    $countries = $_GET['countries'];

    $categories_table = $wpdb->prefix .'categories';

    $list   = '';
    $output = '';

    foreach ($countries as $country) {
        $categories = $wpdb->get_results("SELECT * FROM {$categories_table} WHERE INSTR(language,'{$language}') AND INSTR(method,'\"{$method}\"') AND INSTR(country,'\"{$country}\"')");

        if (! empty($categories)) {
            foreach($categories as $category) {
                $list .= '<option value="'. $category->id .'">'. $category->name .'</option>';
            }
        }
    }

    if (! empty($list)) {
        $output .= '
        <select>
            <option value="">--'. __('Select a Category') .'--</option>
            '. $list .'
        </select>';
    } else {
        $output .= '<h4>'. __('No categories match that selection.', 'spiral');
    }

    echo $output;

    wp_die();
}

/** Save a category */
add_action('wp_ajax_save_category', 'spiral_save_category');
add_action('wp_ajax_nopriv_save_category', 'spiral_save_category');

function spiral_save_category() {
    global $wpdb;

    $id        = $_POST['id'];
    $name      = $_POST['name'];
    $language  = $_POST['language'];
    $method    = $_POST['method'];
    $country   = $_POST['country'];
    // $methods   = json_encode($method);
    $method    = str_replace('"', '', $method);
    $countries = json_encode($country);

    $categories_table = $wpdb->prefix .'categories';

    if (strpos($countries,'"0"')) {
        $countries = '["0"]';
    }

    $data = [
        'name'     => $name,
        'language' => $language,
        'method'   => $method,
        'country'  => $countries
    ];

    if (empty($id)) {
        $save_category = $wpdb->insert($categories_table, $data);

        if ($save_category) {
            echo __('Category successfully added!', 'spiral');
        } else {
            echo __('There was an error adding the category. Data hasn\'t been changed.', 'spiral');
        }
    } else {
        $save_category = $wpdb->update($categories_table, $data, ['id' => $id]);

        if ($save_category) {
            echo __('Category successfully saved!', 'spiral');
        } else {
            echo __('There was an error saving the category. Data hasn\'t been changed.', 'spiral');
        }
    }

    wp_die();
}

/** Delete a category */
add_action('wp_ajax_delete_category', 'spiral_delete_category');
add_action('wp_ajax_nopriv_delete_category', 'spiral_delete_category');

function spiral_delete_category() {
    global $wpdb;

    $id = $_POST['id'];

    if (! empty($id)) {
        $categories_table = $wpdb->prefix .'categories';
        $delete_category  = $wpdb->delete($categories_table, ['id' => $id]);

        if ($delete_category) {
            echo __('Category successfully deleted!', 'spiral');
        } else {
            echo __('There was an error deleting the category.', 'spiral');
        }
    } else {
        echo __('No category was selected to be deleted', 'spiral');
    }

    wp_die();
}


/**
* Scripts
*/

/** Get scripts dropdown */
add_action('wp_ajax_get_scripts_dropdown', 'spiral_get_scripts_dropdown');
add_action('wp_ajax_nopriv_get_scripts_dropdown', 'spiral_get_scripts_dropdown');

function spiral_get_scripts_dropdown() {
    global $wpdb;

    $scripts_table = $wpdb->prefix .'scripts';

    $language  = $_GET['language'];
    $method    = $_GET['method'];
    $countries = $_GET['country'];
    $category  = $_GET['category'];

    if (empty($language)) {
        $language = 0;
    }

    foreach ($countries as $country) {
        $scripts = $wpdb->get_results("SELECT * FROM {$scripts_table} WHERE language = {$language} AND method = {$method} AND category = {$category} AND INSTR(country,\"{$country}\")");

        if (! empty($scripts) && $scripts !== '') {
            foreach($scripts as $script) {
                $list .= '<option value="'. $script->id .'">'. $script->title .'</option>';
            }
        } else {
            $scripts = $wpdb->get_results("SELECT * FROM {$scripts_table} WHERE method = {$method} AND category = {$category} AND INSTR(country,\"{$country}\")");

            if (! empty($scripts) && $scripts !== '') {
                foreach($scripts as $script) {
                    $list .= '<option value="'. $script->id .'">'. $script->title .'</option>';
                }
            }
        }
    }

    if (! empty($list) && $list !== '') {
        $output = '<select>';
        $output .= '<option value="">--'. __('Select a Script') .'--</option>';
        $output .= $list;
        $output .= '</select>';
    } else {
        $output .= '<h4>'. __('No scripts for this selection, but you can add one now.'. '</h4>');
    }

    echo $output;

    wp_die();
}

/** Get script data */
add_action('wp_ajax_get_script_data', 'spiral_get_script_data');
add_action('wp_ajax_nopriv_get_script_data', 'spiral_get_script_data');

function spiral_get_script_data() {
    global $wpdb;

    $id = $_GET['id'];

    $scripts_table = $wpdb->prefix .'scripts';
    $script        = $wpdb->get_results("SELECT * FROM {$scripts_table} WHERE id = {$id}");

    $output = [
        'id'    => $script[0]->id,
        'title' => $script[0]->title,
        'help'  => $script[0]->help,
        'text'  => $script[0]->text
    ];

    $output = json_encode($output);

    echo $output;

    wp_die();
}

/** Search scripts */
add_action('wp_ajax_search_scripts', 'spiral_search_scripts');
add_action('wp_ajax_nopriv_search_scripts', 'spiral_search_scripts');

function spiral_search_scripts() {
    global $wpdb;

    $keyword = $_GET['keyword'];
    $country = $_GET['country'];

    $scripts_table    = $wpdb->prefix .'scripts';

    $scripts = $wpdb->get_results("SELECT * FROM {$scripts_table} WHERE INSTR(country,'\"{$country}\"') AND (INSTR(title,'{$keyword}') OR INSTR(help,'{$keyword}')  OR INSTR(text,'{$keyword}'))");

    if (! empty($scripts)) {
        $output = '';
        $output .= '<div class="box box-search">';
        $output .= '<h4>'. __('Search Results', 'spiral') .'</h4>';
        $output .= '<ul class="categories">';

        foreach ($scripts as $this_script) {
            $output .= '<li class="script '. $new .'"><a href="script?id='. $this_script->id.'">'. $this_script->title .'</a></li>';
        }

        $output .= '</ul>';
        $output .= '</div>';

        echo $output;
    } else {
        echo __('<p>No scripts found with that selection.</p>', 'spiral');
    }

    wp_die();
}

/** Save a script */
add_action('wp_ajax_save_script', 'spiral_save_script');
add_action('wp_ajax_nopriv_save_script', 'spiral_save_script');

function spiral_save_script() {
    global $wpdb;

    $method    = $_POST['method'];
    $countries = $_POST['country'];
    $category  = $_POST['category'];
    $id        = $_POST['id'];
    $title     = stripslashes_deep($_POST['title']);
    $help      = stripslashes_deep($_POST['help']);
    $text      = stripslashes_deep($_POST['text']);
    $countries = json_encode($countries);

    $scripts_table = $wpdb->prefix .'scripts';

    if (! empty($method) && $method !== '' &&
        ! empty($countries) && $countries !== '' &&
        ! empty($category) && $category !== '' &&
        ! empty($title) && $title !== '' &&
        ! empty($help) && $help !== '' &&
        ! empty($text) && $text !== '') {
        $data = [
            'method'   => $method,
            'country'  => $countries,
            'category' => $category,
            'title'    => $title,
            'help'     => $help,
            'text'     => $text
        ];

        if (empty($id)) {
            $save_script = $wpdb->insert($scripts_table, $data);

            if ($save_script) {
                echo __('Script successfully saved!', 'spiral');
            } else {
                echo __('There was an error saving the script.', 'spiral');
            }
        } else {
            $save_script = $wpdb->update($scripts_table, $data, ['id' => $id]);

            if ($save_script) {
                echo __('Script successfully updated!', 'spiral');
            } else {
                echo __('There was an error updating the script.', 'spiral');
            }
        }
    } else {
        echo __('Please fill all the fields and try again', 'spiral');
    }

    wp_die();
}

/** Delete a script */
add_action('wp_ajax_delete_script', 'spiral_delete_script');
add_action('wp_ajax_nopriv_delete_script', 'spiral_delete_script');

function spiral_delete_script() {
    global $wpdb;

    $id = $_POST['id'];

    if (! empty($id) && $id !== '') {
        $scripts_table = $wpdb->prefix .'scripts';
        $delete_script = $wpdb->delete($scripts_table, ['id' => $id]);

        if ($delete_script) {
            echo __('Script successfully deleted!', 'spiral');
        } else {
            echo __('There was an error deleting the script.', 'spiral');
        }
    } else {
        echo __('No script was selected to be deleted.', 'spiral');
    }

    wp_die();
}


/**
 * FAQs
 */

/** Save FAQ */
add_action('wp_ajax_save_faq', 'spiral_save_faq');
add_action('wp_ajax_nopriv_save_faq', 'spiral_save_faq');

function spiral_save_faq() {
    global $wpdb;

    if (isset($_POST['title'])) {
        $id    = $_POST['id'];
        $title = $_POST['title'];
        $text  = stripslashes_deep($_POST['text']);

        $faqs_table = $wpdb->prefix .'faqs';

        $data = [
            'title' => $title,
            'text'  => $text
        ];

        if (empty($id)) {
            $save_faq = $wpdb->insert($faqs_table, $data);

            if ($save_faq) {
                echo __('FAQ successfully saved!', 'spiral');
            } else {
                echo __('There was an error saving the FAQ.', 'spiral');
            }
        } else {
            $save_faq = $wpdb->update($faqs_table, $data, ['id' => $id]);

            if ($save_faq) {
                echo __('FAQ successfully updated!', 'spiral');
            } else {
                echo __('There was an error updating the FAQ.', 'spiral');
            }
        }
    } else {
        echo __('Please fill all the fields and try again', 'spiral');
    }

    wp_die();
}

/** Delete FAQ */
add_action('wp_ajax_delete_faq', 'spiral_delete_faq');
add_action('wp_ajax_nopriv_delete_faq', 'spiral_delete_faq');

function spiral_delete_faq() {
    global $wpdb;

    if (isset($_POST['id'])) {
        $id = $_POST['id'];

        $faqs_table = $wpdb->prefix .'faqs';
        $delete_faq = $wpdb->delete($faqs_table, ['id' => $id]);

        if ($delete_faq) {
            echo __('FAQ successfully deleted!', 'spiral');
        } else {
            echo __('There was an error deleting the FAQ.', 'spiral');
        }
    } else {
        echo __('No FAQ was selected to be deleted.', 'spiral');
    }

    wp_die();
}

/** Get FAQs list */
add_action('wp_ajax_get_faqs_dropdown', 'spiral_get_faqs_dropdown');
add_action('wp_ajax_nopriv_get_faqs_dropdown', 'spiral_get_faqs_dropdown');

function spiral_get_faqs_dropdown() {
    global $wpdb;

    $faqs_table = $wpdb->prefix .'faqs';
    $faqs       = $wpdb->get_results("SELECT * FROM {$faqs_table}");

    $faqs_list = '';
    $output    = '';

    if (! empty($faqs)) {
        foreach($faqs as $faq) {
            $faqs_list .= '<option value="'. $faq->id .'">'. $faq->title .'</option>';
        }
    }

    if (! empty($faqs_list)) {
        $output .= '<select name="faqs" id="faq-list">';
        $output .= '<option value="">-- '. __('Select a FAQ to edit', 'spiral') .' --</option>';
        $output .= $faqs_list;
        $output .= '</select>';
    } else {
        $output .= '<h4>'. __('No FAQs available yet. You can add some below.'. '</h4>');
    }

    echo $output;

    wp_die();
}

/** Get FAQ data */
add_action('wp_ajax_get_faq_data', 'spiral_get_faq_data');
add_action('wp_ajax_nopriv_get_faq_data', 'spiral_get_faq_data');

function spiral_get_faq_data() {
    global $wpdb;

    if (isset($_GET['id'])) {
        $id = $_GET['id'];

        $faqs_table = $wpdb->prefix .'faqs';
        $faq        = $wpdb->get_results("SELECT * FROM {$faqs_table} WHERE id = {$id}");

        $output = [
            'id'    => $faq[0]->id,
            'title' => $faq[0]->title,
            'text'  => $faq[0]->text
        ];
    }

    $output = json_encode($output);

    echo $output;

    wp_die();
}


/**
 * Settings
 */

/** Save user meta */
add_action('wp_ajax_save_user', 'spiral_save_user');
add_action('wp_ajax_nopriv_save_user', 'spiral_save_user');

function spiral_save_user() {
    global $wpdb;

    $id      = $_POST['id'];
    $level   = $_POST['level'];
    $country = $_POST['country'];

    $save_user_level   = update_user_meta($id, 'sms_level', $level);
    $save_user_country = update_user_meta($id, 'sms_country', $country);

    if ($save_user_level || $save_user_country) {
        echo __('User successfully saved!', 'spiral');
    } else {
        echo __('There was an error saving the user.', 'spiral');
    }

    wp_die();
}

/** Image uploader */
function spiral_image_uploader() {
    $option_name   = get_option('sms_logo');
    $default_image = get_stylesheet_directory_uri() .'/assets/images/logo.png';

    if (! empty($option_name)) {
        $image_attributes = wp_get_attachment_image_src($option_name, 'full');
        $image_src        = $image_attributes[0];
        $image_value      = $option_name;
    } else {
        $image_src   = $default_image;
        $image_value = '';
    }
    ?>

    <div class="upload">
        <form method="post">
            <img data-src="<?= $default_image; ?>" src="<?= $image_src; ?>" class="image-src">
            <input type="hidden" name="<?= $option_name; ?>" value="<?= $image_value; ?>" class="image-url">
            <div>
                <button type="submit" class="image-upload"><?= __('Upload', 'spiral'); ?></button>
                <button type="submit" class="image-save"><?= __('Save', 'spiral'); ?></button>
                <button type="submit" class="image-delete">&times;</button>
            </div>
        </form>
    </div>
    <?php
}

/** Save logo image */
add_action('wp_ajax_save_image', 'spiral_save_image');
add_action('wp_ajax_nopriv_save_image', 'spiral_save_image');

function spiral_save_image() {
    $image = $_POST['image'];

    $save_image = update_option('sms_logo', $image, true);

    if ($save_image) {
        echo __('Image successfully saved!', 'spiral');
    } else {
        echo __('There was an error saving the image.', 'spiral');
    }

    wp_die();
}

/** Delete logo image */
add_action('wp_ajax_delete_image', 'spiral_delete_image');
add_action('wp_ajax_nopriv_delete_image', 'spiral_delete_image');

function spiral_delete_image() {
    $delete_image = delete_option('sms_logo');

    if ($delete_image) {
        echo __('Image successfully removed!', 'spiral');
    } else {
        echo __('There was an error removing the image.', 'spiral');
    }

    wp_die();
}

/** Save color */
add_action('wp_ajax_save_color', 'spiral_save_color');
add_action('wp_ajax_nopriv_save_color', 'spiral_save_color');

function spiral_save_color() {
    $color = $_POST['color'];
    $name  = $_POST['name'];

    $name = str_replace('-', '_', $name);

    $save_color = update_option($name, $color, true);

    if ($save_color) {
        echo __('Color successfully saved!', 'spiral');
    } else {
        echo __('There was an error saving the color.', 'spiral');
    }

    wp_die();
}


/**
 * Helper Functions
 */

/** Check selected options */
function check_selected($id, $selection = '') {
    if ($selection != '') {
        if (is_array($selection)) {
            foreach ($selection as $selected) {
                if ($id == $selected) {
                    return ' selected';
                }
            }
        } else {
            if ($id == $selection) {
                return ' selected';
            }
        }
    }
}

/** Display a message asking to fill required fills */
add_action('wp_ajax_required_fields', 'spiral_required_fields');
add_action('wp_ajax_nopriv_required_fields', 'spiral_required_fields');

function spiral_required_fields() {
    echo __('Please fill all the fields and try again', 'spiral');

    wp_die();
}


/**
 * Frontend
 */

/** Get languages */
add_action('wp_ajax_get_languages_frontend', 'spiral_get_languages_frontend');
add_action('wp_ajax_nopriv_get_languages_frontend', 'spiral_get_languages_frontend');

function spiral_get_languages_frontend() {
    global $wpdb;

    $languages_table = $wpdb->prefix .'languages';
    $languages       = $wpdb->get_results("SELECT * FROM {$languages_table}");

    $output = '<select>';

    if (! empty($languages)) {
        $output .= '<option value="">--'. __('Select a Language', 'spiral') .'--</option>';
        $output .= '<option value="0">'. __('ALL LANGUAGES', 'spiral') .'</option>';

        foreach ($languages as $language) {
            $output .= '<option value="'. $language->id .'">'. $language->name .'</option>';
        }
    } else {
        $output .= '<option value="">--'. __('No available languages', 'spiral') .'--</option>';
    }

    $output .= '</select>';

    echo $output;

    wp_die();
}

/** Get methods */
add_action('wp_ajax_get_methods_frontend', 'spiral_get_methods_frontend');
add_action('wp_ajax_nopriv_get_methods_frontend', 'spiral_get_methods_frontend');

function spiral_get_methods_frontend() {
    global $wpdb;

    $methods_table = $wpdb->prefix .'methods';
    $methods       = $wpdb->get_results("SELECT * FROM {$methods_table}");

    $output = '<select>';

    if (! empty($methods) && $methods !== '') {
        $output .= '<option value="">--'. __('Select a Method', 'spiral') .'--</option>';

        foreach($methods as $method) {
            $output .= '<option value="'. $method->id .'">'. $method->name .'</option>';
        }
    } else {
        $output .= '<option>--'. __('No available methods', 'spiral') .'--</option>';
    }

    $output .= '</select>';

    echo $output;

    wp_die();
}

/** Get countries */
add_action('wp_ajax_get_countries_frontend', 'spiral_get_countries_frontend');
add_action('wp_ajax_nopriv_get_countries_frontend', 'spiral_get_countries_frontend');

function spiral_get_countries_frontend() {
    global $wpdb;

    $countries_table = $wpdb->prefix .'countries';
    $countries       = $wpdb->get_results("SELECT * FROM {$countries_table}");

    $output = '<select>';

    if (! empty($countries) && $countries !== '') {
        $output .= '<option value="">--'. __('Select a Country', 'spiral') .'--</option>';

        foreach($countries as $country) {
            $output .= '<option value="'. $country->id .'">'. $country->name .'</option>';
        }
    } else {
        $output .= '<option>--'. __('No Available Countries', 'spiral') .'--</option>';
    }

    $output .= '</select>';

    echo $output;

    wp_die();
}

/** Get a list of scripts grouped by cateogory */
add_action('wp_ajax_get_boxes_frontend', 'spiral_get_boxes_frontend');
add_action('wp_ajax_nopriv_get_boxes_frontend', 'spiral_get_boxes_frontend');

function spiral_get_boxes_frontend() {
    global $wpdb;

    $language = $_GET['language'];
    $method   = $_GET['method'];
    $country  = $_GET['country'];

    $categories_table = $wpdb->prefix .'categories';
    $scripts_table    = $wpdb->prefix .'scripts';

    if ($language == 0) {
        $categories       = $wpdb->get_results("SELECT * FROM {$categories_table} WHERE INSTR(method,'\"{$method}\"') AND INSTR(country,'\"{$country}\"')");
    } else {
        $categories       = $wpdb->get_results("SELECT * FROM {$categories_table} WHERE INSTR(language,'{$language}') AND INSTR(method,'\"{$method}\"') AND INSTR(country,'\"{$country}\"')");
    }

    if (isset($_COOKIE['scripts'])) {
        $cookies = $_COOKIE['scripts'];
        $cookies = stripslashes($cookies);
    }

    if (! empty($categories)) {
        $output = '';

        foreach ($categories as $this_category) {
            $output .= '<div class="box">';
            $output .= '<h4>'. $this_category->name .'</h4>';
            $output .= '<ul class="categories">';

            $scripts = $wpdb->get_results("SELECT * FROM {$scripts_table} WHERE method = {$method} AND category = {$this_category->id}");

            foreach ($scripts as $this_script) {
                $new         = '';
                $script_find = strrpos($cookies, '"'. $this_script->id.'"');

                if ($script_find === false && strtotime($this_script->created) + 16 * 24 * 60 * 60 > time()) {
                    $new = 'script-new';
                }

                if ($this_script->created < $this_script->modified) {
                    $new = 'script-updated';
                }

                $output .= '<li class="script '. $new .'"><a href="script?id='. $this_script->id.'">'. stripslashes($this_script->title) .'</a></li>';
            }

            $output .= '</ul>';
            $output .= '</div>';
        }

        echo $output;
    } else {
        echo __('<p>No scripts found with that selection.</p>', 'spiral');
    }

    wp_die();
}

/** Get scripts list */
add_action('wp_ajax_get_scripts_list', 'spiral_get_scripts_list');
add_action('wp_ajax_nopriv_get_scripts_list', 'spiral_get_scripts_list');

function spiral_get_scripts_list() {
    global $wpdb;

    $script_new  = $_POST['script'];

    if (isset($_COOKIE['scripts'])) {
        $cookies = json_decode(stripslashes($_COOKIE['scripts']),true);
        array_push($cookies, $script_new);
    } else {
        $cookies = array($script_new);
    }

    echo json_encode($cookies);

    wp_die();
}

/** Get categories */
add_action('wp_ajax_get_categories_frontend', 'spiral_get_categories_frontend');
add_action('wp_ajax_nopriv_get_categories_frontend', 'spiral_get_categories_frontend');

function spiral_get_categories_frontend() {
    global $wpdb;

    $method  = $_POST['method'];
    $country = $_POST['country'];

    $categories_table = $wpdb->prefix .'categories';

    if ($country == 0) {
        $categories       = $wpdb->get_results("SELECT * FROM {$categories_table} WHERE INSTR(method,'\"{$method}\"')");
    } else {
        $categories       = $wpdb->get_results("SELECT * FROM {$categories_table} WHERE INSTR(method,'\"{$method}\"') AND INSTR(country,'\"{$country}\"')");
    }

    if (! empty($categories) && $categories !== '') {
        $output = '<select class="categories">';
        $output .= '<option value="">--'. __('Select a Category') .'--</option>';
        $output .= '<option value="0">'. __('List all Categories and Scripts') .'</option>';

        foreach($categories as $category) {
            $output .= '<option value="'. $category->id .'">'. $category->name .'</option>';
        }

        $output .= '</select>';
    } else {
        $output .= '<h3>'. __('No available categories with that selection.') .'</h3>';
    }

    echo $output;

    wp_die();
}

/** Get scripts */
add_action('wp_ajax_get_scripts_frontend', 'spiral_get_scripts_frontend');
add_action('wp_ajax_nopriv_get_scripts_frontend', 'spiral_get_scripts_frontend');

function spiral_get_scripts_frontend() {
    global $wpdb;

    $method   = $_POST['method'];
    $country  = $_POST['country'];
    $category = $_POST['category'];

    $categories_table = $wpdb->prefix .'categories';
    $scripts_table    = $wpdb->prefix .'scripts';

    $output = '';

    if ($category == 0) {
        $categories = $wpdb->get_results("SELECT * FROM {$categories_table} WHERE INSTR(method,'\"{$method}\"') AND INSTR(country,\"{$country}\")");

        foreach ($categories as $this_category) {
          echo '<h2>'. $this_category->name .'</h2>';

          $scripts = $wpdb->get_results("SELECT * FROM {$scripts_table} WHERE method = {$method} AND category = {$this_category->id}");

          foreach ($scripts as $this_script) {
            echo '<h3>'. $this_script->title .'</h3>';
            echo '<div class="script-help">'. $this_script->help .'</div>';
            echo '<div class="script-text">'. htmlspecialchars_decode($this_script->text) .'</div>';
            echo '<button type="button" class="script-copy">'. __('Copy', 'spiral') .'</button>';
          }

        }
    } else {
      if ($country === 0) {
          $scripts = $wpdb->get_results("SELECT * FROM {$scripts_table} WHERE method = {$method} AND category = {$category}");
      } else {
          $scripts = $wpdb->get_results("SELECT * FROM {$scripts_table} WHERE method = {$method} AND category = {$category} AND INSTR(country,\"{$country}\")");
      }

      if (! empty($scripts) && $scripts !== '') {
          foreach($scripts as $script) {
              $list .= '<option value="'. $script->id .'">'. $script->title .'</option>';
          }
      }

      if (! empty($list) && $list !== '') {
          $output = '<select class="scripts">';
          $output .= '<option value="">--'. __('Select a Script') .'--</option>';
          $output .= $list;
          $output .= '</select>';
      } else {
          $output .= '<h4>'. __('No scripts available for this selection.'. '</h4>');
      }
    }

    echo $output;

    wp_die();
}

/** Get a single script */
add_action('wp_ajax_get_script_frontend', 'spiral_get_script_frontend');
add_action('wp_ajax_nopriv_get_script_frontend', 'spiral_get_script_frontend');

function spiral_get_script_frontend() {
    global $wpdb;

    if (isset($_POST['id']) && $_POST['id'] !== '') {
        $id = $_POST['id'];

        $scripts_table = $wpdb->prefix .'scripts';
        $script        = $wpdb->get_results("SELECT * FROM {$scripts_table} WHERE id = {$id}");
    }

    $output .= '<h2>'. $script[0]->title .'</h2>';
    $output .= '<div class="script-help">'. $script[0]->help .'</div>';
    $output .= '<div class="script-text">'. htmlspecialchars_decode($script[0]->text) .'</div>';
    $output .= '<button type="button" class="script-copy">'. __('Copy', 'spiral') .'</button>';

    echo $output;

    wp_die();
}
