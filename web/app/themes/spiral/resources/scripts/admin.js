/** Import style */
import '../styles/admin.styl';

/** Variables */
const required_fields = 'Please fill all the fields and try again.';


(function($) {
  /**
   * Accordion
   */
  $(document).on('click', 'h3', function () {
    if ($(this).next().is(':visible')) {
      $(this).next().slideUp(200);
      $(this).removeClass('active');
    } else {
      $(this).next().slideDown(200).parent().siblings().find('.accordion-content').slideUp(200);
      $(this).addClass('active').siblings().removeClass('active');
    }
  });

  /**
   * Languages
   */

  $('#languages').on('click', '.language-add', function () {
    let languageNew = $(this).prev().find('tr').last().clone();

    languageNew.find('input').val('');
    languageNew.appendTo($(this).prev());
  });

  $('#languages').on('click', '.language-save', function() {
    let id   = $(this).parent().parent().parent().find('.language-id').val();
    let name = $(this).parent().parent().parent().find('.language-name').val();

    if (name != null) {
      $.disable('#languages');

      let data = {
        action: 'save_language',
        id    : id,
        name  : name
      };

      $.post(wpajax.ajaxurl, data, function (response) {
        $.show_message(response);
      }).done(function () {
        $.get_languages();
        $.get_languages_dropdown();
        $.get_categories();
        $.enable('#languages');

        console.log('Save language requested.');
      });
    } else {
      $.show_message(required_fields);
    }
  });

  $('#languages').on('click', '.language-delete', function () {
    let id = $(this).parent().parent().parent().find('.language-id').val();

    if (id != null) {
      if (confirm('Are you sure?')) {
        $.disable('#languages');

        let data = {
          action: 'delete_language',
          id    : id
        };

        $.post(wpajax.ajaxurl, data, function (response) {
          $.show_message(response);
        }).done(function () {
          $.get_languages();
          $.get_languages_dropdown();
          $.get_categories();
          $.enable('#languages');

          console.log('Delete language requested.');
        });
      } else {
        console.log('Delete request cancelled.');
      }
    }
  });

  $.get_languages = function () {
    let data = {
      action: 'get_languages'
    };

    $.get(wpajax.ajaxurl, data, function (response) {
      $('#languages .accordion-content').html(response);
    }).done(function () {
      console.log('Language list requested.');
    });
  }

  $.get_languages_dropdown = function () {
    let data = {
      action: 'get_languages_dropdown'
    };

    $.get(wpajax.ajaxurl, data, function (response) {
      $('#languages-dropdown').html(response).fadeIn();
    }).done(function () {
      console.log('Language dropdown updated.');
    });
  }


  /**
   * Methods
   */

  $('#methods').on('click', '.method-add', function () {
    let methodNew = $(this).prev().find('tr').last().clone();

    methodNew.find('input').val('');
    methodNew.appendTo($(this).prev());
  });

  $('#methods').on('click', '.method-save', function () {
    let id   = $(this).parent().parent().parent().find('.method-id').val();
    let name = $(this).parent().parent().parent().find('.method-name').val();

    if (name != null) {
      $.disable('#methods');

      let data = {
        action: 'save_method',
        id    : id,
        name  : name
      };

      $.post(wpajax.ajaxurl, data, function (response) {
        $.show_message(response);
      }).done(function () {
        $.get_methods();
        $.get_methods_dropdown();
        $.get_categories();
        $.enable('#methods');

        console.log('Save method requested.');
      });
    } else {
      $.show_message(required_fields);
    }
  });

  $('#methods').on('click', '.method-delete', function () {
    let id = $(this).parent().parent().parent().find('.method-id').val();

    if (id != null) {
      if (confirm('Are you sure?')) {
        $.disable('#methods');

        let data = {
          action: 'delete_method',
          id    : id
        };

        $.post(wpajax.ajaxurl, data, function (response) {
          $.show_message(response);
        }).done(function () {
          $.get_methods();
          $.get_methods_dropdown();
          $.get_categories();
          $.enable('#methods');

          console.log('Delete method requested.');
        });
      } else {
        console.log('Delete request cancelled.');
      }
    }
  });

  $.get_methods = function () {
    let data = {
      action: 'get_methods'
    };

    $.get(wpajax.ajaxurl, data, function (response) {
      $('#methods .accordion-content').html(response);
    }).done(function () {
      console.log('Method list requested.');
    });
  }

  $.get_methods_dropdown = function () {
    let data = {
      action: 'get_methods_dropdown'
    };

    $.get(wpajax.ajaxurl, data, function (response) {
      $('#methods-dropdown').html(response).fadeIn();
    }).done(function () {
      console.log('Method dropdown updated.');
    });
  }


  /**
   * Countries
   */

  $('#countries').on('click', '.country-add', function () {
    let countryNew = $(this).prev().find('tr').last().clone();

    countryNew.find('input').val('');
    countryNew.appendTo($(this).prev());
  });

  $('#countries').on('click', '.country-save', function () {
    let id   = $(this).parent().parent().parent().find('.country-id').val();
    let name = $(this).parent().parent().parent().find('.country-name').val();

    if (name != null) {
      $.disable('#countries');

      let data = {
        action: 'save_country',
        id    : id,
        name  : name
      };

      $.post(wpajax.ajaxurl, data, function (response) {
        $.show_message(response);
      }).done(function () {
        $.get_countries();
        $.get_countries_dropdown();
        $.get_categories();
        $.enable('#countries');

        console.log('Save country requested.');
      });
    } else {
      $.show_message(required_fields);
    }
  });

  $('#countries').on('click', '.country-delete', function () {
    let id = $(this).parent().parent().parent().find('.country-id').val();

    if (id != null) {
      if (confirm('Are you sure?')) {
        $.disable('#countries');

        let data = {
          action: 'delete_country',
          id    : id
        };

        $.post(wpajax.ajaxurl, data, function (response) {
          $.show_message(response);
        }).done(function () {
          $.get_countries();
          $.get_countries_dropdown();
          $.get_categories();
          $.enable('#countries');

          console.log('Delete country requested.');
        });
      } else {
        console.log('Delete request cancelled.');
      }
    }
  });

  $.get_countries = function () {
    let data = {
      action: 'get_countries'
    };

    $.get(wpajax.ajaxurl, data, function (response) {
      $('#countries .accordion-content').html(response);
    }).done(function () {
      console.log('Country list requested.');
    });
  }

  $.get_countries_dropdown = function () {
    let data = {
      action: 'get_countries_dropdown'
    };

    $.get(wpajax.ajaxurl, data, function (response) {
      $('#countries-dropdown').html(response).fadeIn();
    }).done(function () {
      console.log('Country dropdown updated.');
    });
  }


  /**
   * Categories
   */

  $('#categories').on('click', '.category-add', function () {
    let categoryNew = $(this).prev().find('tr').last().clone();

    categoryNew.find('input').val('');
    categoryNew.find('option').removeAttr('selected');
    categoryNew.appendTo($(this).prev());
  });

  $('#categories').on('click', '.category-save', function () {
    let id       = $(this).parent().parent().parent().find('.category-id').val();
    let name     = $(this).parent().parent().parent().find('.category-name').val();
    let language = $(this).parent().parent().parent().find('.category-language').val();
    let method   = $(this).parent().parent().parent().find('.category-method').val();
    let country  = $(this).parent().parent().parent().find('.category-country').val();

    if (name != null && language != null && method != null && country != null) {
      $.disable('#categories');

      let data = {
        action  : 'save_category',
        id      : id,
        name    : name,
        language: language,
        method  : method,
        country : country
      };

      $.post(wpajax.ajaxurl, data, function (response) {
        $.show_message(response);
      }).done(function () {
        $.get_categories();
        $.enable('#categories');

        console.log('Save category requested.');
      });
    } else {
      $.show_message(required_fields);
    }
  });

  $('#categories').on('click', '.category-delete', function () {
    let id = $(this).parent().parent().parent().find('.category-id').val();

    if (id != null) {
      if (confirm('Are you sure?')) {
        $.disable('#categories');

        let data = {
          action: 'delete_category',
          id    : id
        };

        $.post(wpajax.ajaxurl, data, function (response) {
          $.show_message(response);
        }).done(function () {
          $.get_categories();
          $.enable('#categories');

          console.log('Delete category requested.');
        });
      } else {
        console.log('Delete request cancelled.');
      }
    }
  });

  $(document).on('change', '#languages-dropdown select, #methods-dropdown select, #countries-dropdown select', function () {
    let language  = $('#languages-dropdown select').val();
    let method    = $('#methods-dropdown select').val();
    let countries = $('#countries-dropdown select').val();

    if (language != '' && method != '' && countries != '') {
      $.get_categories_dropdown(language, method, countries);
    } else {
      $.reset_script_form();

      $('#categories-dropdown').fadeOut().html('');
      $('#scripts-dropdown').fadeOut().html('');
      $('#script form').fadeOut();
      $('#script-add').fadeOut();
    }
  });

  $.get_categories = function () {
    let data = {
      action: 'get_categories'
    };

    $.get(wpajax.ajaxurl, data, function (response) {
      $('#categories .accordion-content').html(response);
    }).done(function () {
      console.log('Category list requested.');
    });
  }

  $.get_categories_dropdown = function (language, method, countries) {
    let data = {
      action   : 'get_categories_dropdown',
      language : language,
      method   : method,
      countries: countries
    };

    $.get(wpajax.ajaxurl, data, function (response) {
      $('#categories-dropdown').html(response).fadeIn();
    }).done(function () {
      console.log('Category dropdown updated.');
    });
  }


  /**
   * Scripts
   */

  $(document).on('click', '#script-add', function () {
    $.show_script_form();
  });

  $(document).on('click', '#script-save', function () {
    let language    = $('#languages-dropdown select').val();
    let method      = $('#methods-dropdown select').val();
    let country     = $('#countries-dropdown select').val();
    let category    = $('#categories-dropdown select').val();
    let id          = $('#script-id').val();
    let title       = $('#script-title').val();
    let help        = $('#script-help').val();
    let tinyMCEhelp = tinyMCE.get('script-help').getContent();
    let text        = $('#script-text').val();
    let tinyMCEtext = tinyMCE.get('script-text').getContent();

    if (help != tinyMCEhelp) {
      help = tinyMCEhelp;
    }

    if (text != tinyMCEtext) {
      text = tinyMCEtext;
    }

    if (language != null && method != null && country != null && category != null && title != null && help != null && text != null) {
      $.disable('#script');

      let data = {
        action  : 'save_script',
        language: language,
        method  : method,
        country : country,
        category: category,
        id      : id,
        title   : title,
        help    : help,
        text    : text
      };

      $.post(wpajax.ajaxurl, data, function (response) {
        $.show_message(response);
      }).done(function () {
        $.get_scripts_dropdown(language, method, country, category);
        $.enable('#script');

        console.log('Save script request.');
      });
    } else {
      $.show_message(required_fields);
    }
  });

  $(document).on('click', '#script-delete', function () {
    let id = $('#script-id').val();

    if (id != null) {
      if (confirm('Are you sure?')) {
        $.disable('#script');

        let language = $('#languages-dropdown select').val();
        let method   = $('#methods-dropdown select').val();
        let country  = $('#countries-dropdown select').val();
        let category = $('#categories-dropdown select').val();

        let data = {
          action: 'delete_script',
          id    : id
        };

        $.post(wpajax.ajaxurl, data, function (response) {
          $.show_message(response);
        }).done(function () {
          $.get_scripts_dropdown(language, method, country, category);
          $.enable('#script');

          console.log('Delete script request.');
        });
      } else {
        console.log('Delete request cancelled.');
      }
    }
  });

  $(document).on('change', '#categories-dropdown select', function () {
    let language  = $('#languages-dropdown select').val();
    let method    = $('#methods-dropdown select').val();
    let countries = $('#countries-dropdown select').val();
    let category  = $('#categories-dropdown select').val();

    if (language != '' && method != '' && countries != '' && category != '') {
      $.get_scripts_dropdown(language, method, countries, category);
    } else {
      $.reset_script_form();

      $('#scripts-dropdown').fadeOut().html('');
      $('#script form').fadeOut();
      $('#script-add').fadeOut();
    }
  });

  $(document).on('change', '#scripts-dropdown select', function () {
    let id = $('#scripts-dropdown select').val();

    if (id != '') {
      $.show_script_form(id);
    } else {
      $.reset_script_form();

      $('#script form').fadeOut();
    }
  });

  $.get_scripts_dropdown = function (language, method, country, category) {
    let data = {
      action   : 'get_scripts_dropdown',
      language : language,
      method   : method,
      country  : country,
      category : category
    };

    $.get(wpajax.ajaxurl, data, function (response) {
      $('#scripts-dropdown').html(response).fadeIn();
    }).done(function () {
      $('#script form').fadeOut();
      $('#script-add').fadeIn();

      console.log('Script dropdown updated.');
    });

    $.reset_script_form();
  }

  $.show_script_form = function (id = null) {
    let data = {
      action: 'get_script_data',
      id: id
    };

    if (id != null) {
      $.get(wpajax.ajaxurl, data, function (response) {
        let script = JSON.parse(response);

        $('#script-id').val(script.id);
        $('#script-title').val(script.title);
        $('#script-help').val(script.help);
        $('#script-text').val(script.text);

        tinyMCE.get('script-help').setContent(script.help);
        tinyMCE.get('script-text').setContent(script.text);
      });

      console.log('Script data requested.');
    }

    $('#script form').fadeIn();
  }

  $.reset_script_form = function () {
    $('#script-id').val('');
    $('#script-title').val('');
    $('#script-help').val('');
    $('#script-text').val('');

    tinyMCE.get('script-help').setContent('');
    tinyMCE.get('script-text').setContent('');

    console.log('Script form reset.');
  }


  /**
   * FAQs
   */

  $(document).on('click', '#faq-add', function () {
    $.reset_faq_form();
  });

  $(document).on('click', '#faq-save', function () {
    let id          = $('#faq-id').val();
    let title       = $('#faq-title').val();
    let text        = $('#faq-text').val();
    let tinyMCEtext = tinyMCE.get('faq-text').getContent();

    if (text != tinyMCEtext) {
      text = tinyMCEtext;
    }

    if (title != null && text != null) {
      $.disable('#faq');

      let data = {
        action  : 'save_faq',
        id      : id,
        title   : title,
        text    : text
      };

      $.post(wpajax.ajaxurl, data, function (response) {
        $.show_message(response);
      }).done(function (){
        $.get_faqs_dropdown();
        $.enable('#faq');

        console.log('Save FAQ request.');
      });
    } else {
      $.show_message(required_fields);
    }
  });

  $(document).on('click', '#faq-delete', function () {
    let id = $('#faq-id').val();

    if (id != null) {
      if (confirm('Are you sure?')) {
        $.disable('#faq');

        let data = {
          action: 'delete_faq',
          id    : id
        };

        $.post(wpajax.ajaxurl, data, function (response) {
          $.show_message(response);
        }).done(function () {
          $.get_faqs_dropdown();
          $.enable('#faq');

          console.log('Delete FAQ request.');
        });
      } else {
        console.log('Delete request cancelled.');
      }
    }
  });

  $(document).on('change', '#faq-list', function () {
    let faq = $('#faq-list').val();

    if (faq != null) {
      $.show_faq_form(faq);
    } else {
      $.reset_faq_form();
    }
  });

  $.get_faqs_dropdown = function () {
    let data = {
      action: 'get_faqs_dropdown'
    };

    $.get(wpajax.ajaxurl, data, function (response) {
      $('#faqs').html(response).fadeIn();
    });

    $.reset_faq_form();

    console.log('Updating FAQ dropdown.');
  }

  $.show_faq_form = function (id) {
    let data = {
      action: 'get_faq_data',
      id: id
    };

    if (id != null) {
      $.disable('#faq');

      $.get(wpajax.ajaxurl, data, function (response) {
        var faq = JSON.parse(response);

        $('#faq-id').val(faq.id);
        $('#faq-title').val(faq.title);
        $('#faq-text').val(faq.text);

        tinyMCE.get('faq-text').setContent(faq.text);
      }).done( function () {
        $.enable('#faq');
      });

      console.log('Request FAQ data.');
    } else {
      tinyMCE.get('faq-text').setContent('');

      console.log('Clean FAQ form.');
    }
  }

  $.reset_faq_form = function () {
    $('#faq-id').val('');
    $('#faq-title').val('');
    $('#faq-text').val('');
    tinyMCE.get('faq-text').setContent('');

    console.log('Clean FAQ form.');
  }


  /**
   * Colors
   */

  $('.color-select').wpColorPicker();

  $(document).on('click', '.color-save', function () {
    let color = $(this).parent().find('.color-select').val();
    let name  = $(this).parent().find('.color-select').attr('name');

    if (color != null) {
      let data = {
        action: 'save_color',
        color : color,
        name  : name
      }

      $.post(wpajax.ajaxurl, data, function (response) {
        $.show_message(response);
      }).done(function () {
        console.log('Save color requested.');
      });
    } else {
      $.show_message(required_fields);
    }
  });


  /**
   * Users
   */

  $(document).on('click', '.user-save', function () {
    let id      = $(this).parent().find('.user-id').val();
    let level   = $(this).parent().find('.user-level').val();
    let country = $(this).parent().find('.user-country').val();

    if (id != null && level != null && country != null) {
      let data = {
        action : 'save_user',
        id     : id,
        level  : level,
        country: country
      }

      $.post(wpajax.ajaxurl, data, function (response) {
        $.show_message(response);
      }).done(function () {
        console.log('User save requested.');
      });
    } else {
      $.show_message(required_fields);
    }
  });


  /**
   * Images
   */

  $(document).on('click', '.image-upload', function (event) {
    event.preventDefault();

    let mediaUploader;

    if (mediaUploader) {
      mediaUploader.open();
      return;
    }

    mediaUploader = wp.media.frames.file_frame = wp.media({
      title: 'Pick or upload an image',
      button: {
        text: 'Select Image'
      },
      multiple: false
    });

    mediaUploader.on('select', function () {
      let attachment = mediaUploader.state().get('selection').first().toJSON();

      $('.image-url').attr({src: attachment.url});
      $('.image-id').val(attachment.id);

      console.log('Image uploaded.');
    });

    mediaUploader.open();
  });

  $(document).on('click', '.image-save', function (event) {
    event.preventDefault();

    let image = $('.image-id').val();

    if (image != null) {
      let data = {
        action : 'save_image',
        image  : image
      }

      $.post(wpajax.ajaxurl, data, function (response) {
        $.show_message(response);
      }).done(function () {
        console.log('Save image requested.');
      });
    } else {
      $.show_message('Please select an image.');
    }
  });

  $(document).on('click', '.image-delete', function(event) {
    event.preventDefault();

    if (confirm('Are you sure?')) {
      let data = {
        action : 'delete_image'
      }

      $.post(wpajax.ajaxurl, data, function (response) {
        $.show_message(response);
      }).done(function () {
        let image = $('.image-url').attr('data-src');

        $('.image-url').attr('src', image);
        $('.image-id').val('');

        console.log('Remove image requested.');
      });
    } else {
      console.log('Remove image request cancelled.');
    }

    return false;
  });


  /**
   * Helper Functions
   */

  /** Fade In & Out */
  $.disable = function (element) {
    $(element).fadeTo('fast', 0.5).css({'cursor': 'wait'});
    console.log('Wait...');
  }

  $.enable = function (element) {
    $(element).fadeTo('fast', 1).css({'cursor': 'auto'});
    console.log('Done.');
  }

  /** Display Messages */
  $.show_message = function (message) {
    $('#message').html(message).fadeIn().delay(2000).fadeOut();
  }

  /**
   * Setup System on Load
   */
  $(document).ready(function () {
    $.get_languages();
    $.get_methods();
    $.get_countries();
    $.get_categories();
    $.get_languages_dropdown();
    $.get_methods_dropdown();
    $.get_countries_dropdown();

    console.log('SMS loaded.');
  });

})(jQuery);
