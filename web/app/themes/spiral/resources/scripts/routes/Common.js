/** Import dependencies */
import $ from 'jquery';
import Cookie from 'js-cookie';


/** Get languages */
$(document).ready(function () {
  let data = {
    action: 'get_languages_frontend'
  };

  $.post(wpajax.ajaxurl, data, function (response) {
    $('#languages').html(response);
  }).done(function () {
    console.log('Language list requested.');
  });
})


/** Get methods */
$(document).ready(function () {
  let data = {
    action : 'get_methods_frontend'
  };

  $.post(wpajax.ajaxurl, data, function (response) {
    $('#methods').html(response);
  }).done(function () {
    console.log('Method list requested.');
  });
});


/** Get countries */
$(document).ready(function () {
  let data = {
    action : 'get_countries_frontend'
  };

  $.post(wpajax.ajaxurl, data, function (response) {
    $('#countries').html(response);
  }).done(function () {
    console.log('Country list requested.');
  });
});


/** Get categories */
$(document).on('change', '#languages select, #methods select, #countries select', function () {
  let language = $('#languages select').val();
  let method   = $('#methods select').val();
  let country  = $('#countries select').val();

  if (language != '' && method != '' && country != '') {
    let data = {
      action  : 'get_boxes_frontend',
      language: language,
      method  : method,
      country : country
    };

    $.get(wpajax.ajaxurl, data, function (response) {
      $('#boxes').html(response);
    }).done(function () {
      console.log('Script list requested.');
    });

  }

  reset_scripts();
});


/** Save script cookie */
$(document).on('click', '.script a', function() {
  let script_id = $(this).prop('href');
  let script    = script_id.split("=")[1].split(".")[0];

  $(this).parent().removeClass('script-new');

  let data = {
    action : 'get_scripts_list',
    script : script
  };

  $.post(wpajax.ajaxurl, data, function (response) {
    Cookie.set('scripts', response);
  });

  // Cookie.remove('scripts');
});


/** Open script in a popup */
$(document).on('click', '.script a', function (event) {
  event.preventDefault();

  let script = $(this).prop('href');

  console.log(script);
  window.open(script, '_blank', 'titlebar=0,toolbar=no,scrollbars=yes,resizable=yes,width='+screen.width+',height='+screen.height);
});


/** Open and close buttons for side bars */
$(document).on('click', '.button-close', function () {
  $(this).parent().removeClass('opened');
});

$(document).on('click', '.open-faq', function () {
  $('#faq').addClass('opened');
});

$(document).on('click', '.open-clipboard', function () {
  $('#clipboard').addClass('opened');
});


/** FAQ accordion */
$(document).on('click', '#faq h3', function() {
  if ($(this).next().is(':visible')) {
    $(this).next().slideUp(200);
    $(this).removeClass('active');
  } else {
    $(this).next().slideDown(200).siblings('.faq-body').slideUp(200);
    $(this).addClass('active').siblings().removeClass('active');
  }
});


/** Enable search bar after selecting a country */
$(document).on('change', '#countries select', function() {
  let country = $('#countries select').val();

  if (country != '') {
    $('.script-search-field').removeAttr('disabled');
  } else {
    $('.script-search-field').attr('disabled', 'disabled');
  }
});


/** Search scripts */
$(document).on('submit', '.script-search', function (event) {
  event.preventDefault();

  let keyword = $('.script-search-field').val();
  let country = $('#countries select').val();

  if (keyword != '' && country != '') {
    let data = {
      action  : 'search_scripts',
      keyword : keyword,
      country : country
    };

    $.get(wpajax.ajaxurl, data, function (response) {
      $('#boxes').html(response);
    }).done(function () {
      console.log('Script list requested.');
    });
  }
});

$(document).on('click', '.script-search-submit', function (event) {
  event.preventDefault();

  let keyword = $('.script-search-field').val();
  let country = $('#countries select').val();

  if (keyword != '' && country != '') {
    let data = {
      action  : 'search_scripts',
      keyword : keyword,
      country : country
    };

    $.get(wpajax.ajaxurl, data, function (response) {
      $('#boxes').html(response);
    }).done(function () {
      console.log('Script list requested.');
    });
  }
});


/** Reset script list */
function reset_scripts() {
  $('#scripts').html('');
  $('#script').html('');
}


/** Display messages */
function send_message(message) {
  $('#message').html(message).fadeIn().delay(2000).fadeOut();
}


/** Copy textarea to clipboard */
$(document).on('click', '.script-copy', function() {
  var textarea = $(this).prev()[0];

  copyToClipboard(textarea);
});

function copyToClipboard(element) {
  selectText(element);

  var copied;

  try {
    copied = document.execCommand('copy');
  } catch (err) {
    copied = false;
  }

  if (copied) {
    send_message('Script copied to clipboard');
  } else {
    send_message('Failed to copy');
  }

  deselect(element);
}

function selectText(element) {
  if (/INPUT|TEXTAREA/i.test(element.tagName)) {
    element.focus();

    if (element.setSelectionRange) {
      element.setSelectionRange(0, element.value.length);
    } else {
      element.select();
    }

    return;
  }

  var rangeObj, selection;

  if (document.createRange) {
    rangeObj = document.createRange();
    rangeObj.selectNodeContents(element);
    selection = window.getSelection();
    selection.removeAllRanges();
    selection.addRange(rangeObj);
  } else if (document.body.createTextRange) {
    rangeObj = document.body.createTextRange();
    rangeObj.moveToElementText(element);
    rangeObj.select();
  }
}

function deselect(element) {
  if (element && /INPUT|TEXTAREA/i.test(element.tagName)) {
    if ('selectionStart' in element) {
      element.selectionEnd = element.selectionStart;
    }
    element.blur();
  }

  if (window.getSelection) {
    window.getSelection().removeAllRanges();
  } else if (document.selection) {
    document.selection.empty();
  }
}
