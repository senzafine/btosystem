/** Import normalize.css */
import 'normalize.css';

/** Import style */
import '../styles/main.styl';

/** Import functions */
import './routes/common';
import './routes/home';

/** Define sprites folder and files */
const req = require.context('../images/sprites', true, /\.svg$/);

req.keys().forEach(function(key) {
  req(key);
});
