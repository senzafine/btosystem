@extends('layouts.script')

@section('content')

  @php
    $script = $_GET['id'];
  @endphp

  <section id="script">
    <!--<input type="hidden" class="script-id" value="{{ $script }}">-->
    @php
      global $wpdb;

      if ($_GET['id'] && $_GET['id'] !== '') {
          $id            = $_GET['id'];
          $scripts_table = $wpdb->prefix .'scripts';
          $script        = $wpdb->get_results("SELECT * FROM {$scripts_table} WHERE id = {$id}");
      }
    @endphp

    <h2>{!! $script[0]->title !!}</h2>
    <div class="script-help">{!! $script[0]->help !!}</div>
    <div class="script-text">{!! htmlspecialchars_decode($script[0]->text) !!}</div>
    <button type="button" class="script-copy">{!! __('Copy', 'spiral') !!}</button>
  </section>

  <div id="message"></div>

@endsection
