<header class="header-main">
  <div class="container">
    <h1 class="logo">
      <a href="{{ esc_url(home_url('/')) }}" aria-label="{{ bloginfo('name') }}">{{ bloginfo('name') }}</a>
    </h1>

    <div class="navbar-main">
      @include('partials.searchform')
    </div>
  </div>
</header>
