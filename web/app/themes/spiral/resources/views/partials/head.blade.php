<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, shrink-to-fit=no">
  <link rel="apple-touch-icon" sizes="180x180" href="{{ get_stylesheet_directory_uri() }}/assets/images/apple-touch-icon.png?v=Om5W2MRmKJ">
  <link rel="icon" type="image/png" href="{{ get_stylesheet_directory_uri() }}/assets/images/favicon-32x32.png?v=Om5W2MRmKJ" sizes="32x32">
  <link rel="icon" type="image/png" href="{{ get_stylesheet_directory_uri() }}/assets/images/favicon-16x16.png?v=Om5W2MRmKJ" sizes="16x16">
  <link rel="manifest" href="{{ get_stylesheet_directory_uri() }}/assets/images/manifest.json?v=Om5W2MRmKJ">
  <link rel="mask-icon" href="{{ get_stylesheet_directory_uri() }}/assets/images/safari-pinned-tab.svg?v=Om5W2MRmKJ" color="#0066b3">
  <meta name="apple-mobile-web-app-title" content="{{ bloginfo('name') }}">
  <meta name="application-name" content="{{ bloginfo('name') }}">
  <meta name="msapplication-TileColor" content="#0066b3">
  <meta name="msapplication-TileImage" content="{{ get_stylesheet_directory_uri() }}/assets/images/mstile-144x144.png?v=Om5W2MRmKJ">
  <meta name="msapplication-config" content="{{ get_stylesheet_directory_uri() }}/assets/images/browserconfig.xml?v=Om5W2MRmKJ">
  <meta name="theme-color" content="#0066b3">

  @php
    wp_head();
    $image_id        = get_option('sms_logo');
    $color_primary   = get_option('color_primary');
    $color_secondary = get_option('color_secondary');
    $color_font      = get_option('color_font');

    $image_attributes = wp_get_attachment_image_src($image_id, 'full');
    $image_src        = $image_attributes[0];
    $image_width      = $image_attributes[1];
    $image_height     = $image_attributes[2];

    function darken_color($rgb, $darker = 2) {
      $hash = (strpos($rgb, '#') !== false) ? '#' : '';
      $rgb = (strlen($rgb) == 7) ? str_replace('#', '', $rgb) : ((strlen($rgb) == 6) ? $rgb : false);

      if (strlen($rgb) != 6) {
        return $hash.'000000';
      }

      $darker = ($darker > 1) ? $darker : 1;

      list($R16, $G16, $B16) = str_split($rgb, 2);

      $R = sprintf("%02X", floor(hexdec($R16)/$darker));
      $G = sprintf("%02X", floor(hexdec($G16)/$darker));
      $B = sprintf("%02X", floor(hexdec($B16)/$darker));

      return $hash.$R.$G.$B;
    }

    function lighten_color($rgb, $lighter = 2) {
      $hash = (strpos($rgb, '#') !== false) ? '#' : '';
      $rgb = (strlen($rgb) == 7) ? str_replace('#', '', $rgb) : ((strlen($rgb) == 6) ? $rgb : false);

      if (strlen($rgb) != 6) {
        return $hash.'ffffff';
      }

      $lighter = ($lighter > 1) ? $lighter : 1;

      list($R16, $G16, $B16) = str_split($rgb, 2);

      $R = sprintf("%02X", floor(hexdec($R16)/$lighter));
      $G = sprintf("%02X", floor(hexdec($G16)/$lighter));
      $B = sprintf("%02X", floor(hexdec($B16)/$lighter));

      return $hash.$R.$G.$B;
    }
  @endphp

  <style>
    @if (! empty($image_src))
      .header-main h1 a {
        background-image: url({{ $image_src }});
        background-size: contain;
        height: 4rem;
      }
    @endif

    @if (! empty($color_primary))
      body,
      .header-main,
      .script-copy {
        background-color: {{ $color_primary }};
      }

      .script-copy:hover,
      .script-copy:focus,
      .script-copy:active {
        background-color: {{ lighten_color($color_primary) }}
      }

      input[type='search'],
      select {
        background-color: {{ darken_color($color_primary) }};
      }

      input[type='search']:focus,
      input[type='search']:active,
      select:active,
      select:focus,
      select:active option,
      select:focus option {
        background-color: {{ darken_color($color_primary, 3) }};
      }

      .border-primary {
        border-color: {{ $color_primary }};
      }

      .box {
        background-color: {{ darken_color($color_primary) }};
        border-color: {{ darken_color($color_primary) }};
      }

      .open-faq:before,
      .open-clipboard:before {
        color: {{ $color_primary }};
      }
    @endif

    @if (! empty($color_secondary))
      #faq,
      #clipboard,
      .open-faq,
      .open-clipboard,
      .open-faq:hover,
      .open-clipboard:hover,
      .open-faq:focus,
      .open-clipboard:focus,
      .open-faq:active,
      .open-clipboard:active {
        background-color: {{ $color_secondary }};
      }

      .footer-main .button {
        border-color: {{ darken_color($color_secondary) }};
      }

      .footer-main .button:focus,
      .footer-main .button:hover,
      .border-secondary {
        border-color: {{ $color_secondary }};
      }

      .color-secondary {
        color: {{ $color_secondary }};
      }
    @endif

    @if (! empty($color_font))
      body,
      input[type='text'],
      input[type='email'],
      input[type='password'],
      input[type='search'],
      select,
      input[type='text']:focus,
      input[type='email']:focus,
      input[type='password']:focus,
      input[type='search']:focus,
      select:focus,
      input[type='text']:active,
      input[type='email']:active,
      input[type='password']:active,
      input[type='search']:active,
      select:active,
      h2,
      .button-close:before {
        color: {{ $color_font }};
      }
    @endif
  </style>
</head>
