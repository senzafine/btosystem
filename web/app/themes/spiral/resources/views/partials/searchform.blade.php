<form role="search" method="get" class="script-search">
  <label for="search-keyword" class="screen-reader-text">{{ __('Search', 'spiral') }}</label>
  <input type="search" name="search-keyword" placeholder="{{ __('Select a country before searching &hellip;', 'spiral') }}" class="script-search-field" disabled>
  <button class="script-search-submit">
    <svg aria-hidden="true" class="icon icon-search">
      <use xlink:href="{{ get_stylesheet_directory_uri() }}/assets/images/sprite.svg#search"/>
    </svg>
  </button>
</form>
