<footer class="footer-main">
  <div class="container">
    <a class="button" href="{{ bloginfo('url') }}?page_id=93">{{ __('Report a Bug?', 'spiral') }}</a>
    <div class="credits">
      <div class="credits-legal">
        <p>&copy; {{ date('Y') }} <strong>{{ bloginfo('name') }}</strong>. {{ __('All Rights Reserved.', 'spiral') }}</p>
      </div>

      <div class="credits-signature">
        <a href="http://senzafine.net" aria-label="{{ __('Made by Senzafine', 'spiral') }}">
          <svg aria-hidden="true">
            <use xlink:href="{{ get_stylesheet_directory_uri() }}/assets/images/sprite.svg#made"/>
          </svg>
        </a>
      </div>
    </div>
  </div>
</footer>
