<article {{ post_class('summary') }}>
  @if (has_post_thumbnail())
    <figure class="summary-thumbnail">
      <a href="{{ the_permalink() }}">
        {!! get_the_post_thumbnail($post->ID, 'featured_image_thumbnail') !!}
      </a>
    </figure>
  @endif

  <div class="summary-body">
    <header>
      <h1><a href="{{ the_permalink() }}">{{ get_the_title() }}</a></h1>
      @include('partials/meta')
    </header>

    <div class="summary-content">
      <a href="{{ the_permalink() }}" class="summary-more button button-flat button-small">{{ __('Read', 'spiral') }}</a>
    </div>
  </div>
</article>
