@if (is_singular())
  <div class="entry-meta">
    <time class="entry-time" datetime="{{ get_post_time('c', true) }}">{{ get_the_date() }}</time>

    <span class="separator"></span>

    <span class="entry-categories">
      {{ get_the_category_list(' , ') }}
    </span>

    <span class="separator"></span>

    <span class="vcard author entry-author">
      {{ __('Posted by', 'spiral' ) }} <a href="{{ get_author_posts_url(get_the_author_meta('ID')) }}" rel="author">{{ get_the_author() }}</a>
    </span>
  </div>
@else
  <div class="summary-meta">
    <time datetime="{{ get_post_time('c', true) }}">{{ get_the_modified_date() }}</time>
  </div>
@endif
