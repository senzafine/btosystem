@extends('layouts.app')

@section('content')

<div id="message"></div>

<section id="languages"></section>

<section id="methods"></section>

<section id="countries"></section>

<section id="script"></section>

<section id="boxes"></section>

<section id="categories"></section>

<section id="scripts"></section>

<button class="open-faq"></button>

<section id="faq">
  <button class="button-close"></button>
  <div class="content">
    <h2>{{ __('FAQ', 'spiral') }}</h2>
    @php
      global $wpdb;

      $faqs_table = $wpdb->prefix .'faqs';
      $faqs = $wpdb->get_results("SELECT * FROM {$faqs_table}");
    @endphp

    @if (!empty($faqs))
      @foreach($faqs as $faq)
        <h3>{!! $faq->title !!}</h3>
        <div class="faq-body">{!! $faq->text !!}</div>
      @endforeach
    @endif
  </div>
</section>

<button class="open-clipboard"></button>

<section id="clipboard">
  <button class="button-close"></button>
  <div class="content">
    <h2>{{ __('Clipboard', 'spiral') }}</h2>
    <textarea class="clipboard"></textarea>
    <button type="button" class="script-copy">{{ __('Copy', 'spiral') }}</button>
  </div>
</section>

@endsection
