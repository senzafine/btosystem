<!doctype html>
<html @php(language_attributes())>
  @include('partials.head')

  <body id="top" @php(body_class())>
    @php(do_action('get_header'))

    <main class="main">
      <div class="container">
        @yield('content')
      </div>
    </main>

    @php(do_action('get_footer'))
    @php(wp_footer())
  </body>
</html>
