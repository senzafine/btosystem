<!doctype html>
<html @php(language_attributes())>
  @include('partials.head')

  <body id="top" @php(body_class())>
    <!--[if IE]>
      <div class="alert alert-warning">
        {{ __('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'spiral') }}
      </div>
    <![endif]-->

    @php(do_action('get_header'))
    @include('partials.header')

    <main class="main">
      <div class="container">
        @yield('content')
      </div>
    </main>

    @php(do_action('get_footer'))
    @include('partials.footer')
    @php(wp_footer())
  </body>
</html>
