process.noDeprecation = true

const webpack                     = require('webpack');
const path                        = require('path');
const merge                       = require('webpack-merge');
const tasks                       = require('./webpack.config.tasks');
const glob                        = require('glob');
const WebpackNotifierPlugin       = require('webpack-notifier');
const FriendlyErrorsWebpackPlugin = require('friendly-errors-webpack-plugin');

const PATHS = {
  scripts : path.join(__dirname, 'resources/scripts'),
  styles  : path.join(__dirname, 'resources/styles'),
  fonts   : path.join(__dirname, 'resources/fonts'),
  images  : path.join(__dirname, 'resources/images'),
  sprites : path.join(__dirname, 'resources/images/sprites'),
  node    : path.join(__dirname, 'node_modules'),
  build   : path.join(__dirname, 'assets'),
  public  : '/app/themes/spiral/assets/'
};

const commonConfig = merge([
  {
    entry: {
      main   : PATHS.scripts + '/main',
      admin  : PATHS.scripts + '/admin',
      picker : PATHS.scripts + '/picker',
      editor : PATHS.scripts + '/editor',
      login  : PATHS.scripts + '/login',
      vendor : PATHS.scripts + '/vendor'
    },

    output: {
      path      : PATHS.build,
      publicPath: PATHS.public
    },

    plugins: [
      new WebpackNotifierPlugin({
        title: 'Spiral',
        contentImage: path.resolve('./resources/images/icon.png')
      }),

      new FriendlyErrorsWebpackPlugin()
    ],

    resolve: {
      extensions: ['.js','.css','.styl','.php','.svg', '.eot', '.png'],
      modules: [path.join(__dirname, 'src'), 'node_modules']
    }
  },

  tasks.clean(PATHS.build),

  tasks.lintJavascript({
    include: PATHS.scripts
  }),

  tasks.loadScripts({
    include: PATHS.scripts,
    options: {
      presets: ['env'],
      cacheDirectory: true
    }
  }),

  tasks.lintStyles({
    include: PATHS.styles
  }),

  tasks.extractBundles([
    {
      name: 'vendor',

      minChunks: ({ resource }) => (
        resource &&
        resource.indexOf('node_modules') >= 0 &&
        resource.match(/\.js$/)
      )
    },

    {
      name: 'manifest',
      minChunks: Infinity
    }
  ]),

  tasks.loadSprites({
    include: PATHS.sprites
  }),

  tasks.copyAssets(),

  tasks.assets({
    path: PATHS.build
  })
]);

const developmentConfig = merge([
  {
    output: {
      chunkFilename                : 'scripts/[name].js',
      filename                     : 'scripts/[name].js',
      devtoolModuleFilenameTemplate: 'webpack:///[absolute-resource-path]'
    }
  },

  tasks.setFreeVariable(
    'process.env.NODE_ENV',
    'development'
  ),

  tasks.browserSyncServer({
    paths: PATHS,
    host: '192.168.10.10',
    proxy: 'bto.app',
    port: 3000,
    files: [
      'app/**/*.php',
      'resources/views/**/*.php',
      'assets/scripts/**/*.js',
      'assets/styles/**/*.css'
    ]
  }),

  tasks.extractStyles({
    filename: 'styles/[name].css',
    use: [
      'css-loader',
      'resolve-url-loader',
      'stylus-loader',
      tasks.autoprefix()
    ]
  }),

  tasks.sourceMaps({
    type: 'cheap-module-eval-source-map'
  }),

  tasks.loadImages({
    exclude: [
      PATHS.fonts,
      PATHS.sprites
    ],
    options: {
      name: 'images/[name].[ext]'
    }
  }),

  tasks.loadFonts({
    include: [
      PATHS.fonts,
      PATHS.node
    ],
    options: {
      name: 'fonts/[name].[ext]'
    }
  })
]);

const productionConfig = merge([
  {
    output: {
      chunkFilename: 'scripts/[name].[chunkhash:8].js',
      filename     : 'scripts/[name].[chunkhash:8].js'
    },

    recordsPath: path.join(__dirname, 'records.json'),

    plugins: [
      new webpack.HashedModuleIdsPlugin()
    ]
  },

  tasks.setFreeVariable(
    'process.env.NODE_ENV',
    'production'
  ),

  tasks.minifyScripts(),

  tasks.extractStyles({
    filename: 'styles/[name].[contenthash:8].css',
    use: [
      'css-loader',
      'resolve-url-loader',
      'stylus-loader',
      tasks.autoprefix()
    ]
  }),

  tasks.minifyStyles({
    options: {
      discardComments: {
        removeAll: true
      },
      safe: false
    }
  }),

  tasks.sourceMaps({
    type: 'source-map'
  }),

  tasks.loadImages({
    exclude: [
      PATHS.fonts,
      PATHS.sprites
    ],
    options: {
      name: 'images/[name].[hash:8].[ext]'
    }
  }),

  tasks.loadFonts({
    include: [
      PATHS.fonts,
      PATHS.node
    ],
    options: {
      name: 'fonts/[name].[hash:8].[ext]'
    }
  })
]);

module.exports = (env) => {
  if (env == 'production') {
    return merge(commonConfig, productionConfig);
  }

  return merge(commonConfig, developmentConfig);
};
